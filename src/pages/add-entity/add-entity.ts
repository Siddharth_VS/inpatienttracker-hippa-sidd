import { Component } from '@angular/core';
import { NavParams, NavController, ModalController, ToastController, ViewController } from 'ionic-angular';
import { EntityLookupPage } from '../entity-lookup/entity-lookup';
import { Helper } from '../../providers/helper';
import * as moment from 'moment';
import * as _ from 'underscore';

@Component({
  selector: 'page-add-entity',
  templateUrl: 'add-entity.html'
})

export class AddEntitiyPage {
  date: any = moment().format();
  maxDate: any = new Date();
  type: string;
  mode: string;
  title: string;
  notes: any = {};
  query: any = '';
  codeTypes: any = {
    'procedures': 'CPT',
    'diagnosis': 'ICD',
    'em_code': 'VISIT',
  }
  entitiesList: any = [];
  selectedEntities: any = [];
  selectedLookupEntities: any = [];
  favoriteList: any = {};
  searchKeys = ['name', 'code'];
  isNoteEditable: boolean = false;
  moment = moment;
  favoriteRef: any;
  uid: any;
  patientId: any;
  existingUserFavourites: any = [];
  userFavEntities: any = {};
  specializationId: string = '2';
  patientService: any;
  notesCallback: any;
  admissionDate: any;
  max: any = moment().format('');
  min: any = moment().format('');
  _ = _;

  constructor(
    public helper: Helper,
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public params: NavParams,
    public toastCtrl: ToastController,
    public view: ViewController
  ) {
    this.patientService = this.params.get("patientServiceRef");
    this.notesCallback = this.params.get("notesCallback");
    this.type = this.params.get("type").toLowerCase();
    this.title = this.type.split('_').join(' ');
    this.patientId = this.params.get("patientId");
    this.admissionDate = this.params.get("admissionDate");
    this.min = moment(this.admissionDate).format('');
    this.mode = this.params.get('mode');
    this.date = this.selectedEntities.length ? this.selectedEntities[0].date : moment().format();
    this.title = this.type == 'notes' ? 'Note' : this.title;
    this.selectedEntities = this.params.get('items') || [];

    if (this.type == 'notes') {
      this.notes = _.clone(this.params.get("notes") || {});
      this.date = moment(this.notes.date).format();
    }
    else {
      this.getToplookupCodes();
    }
  }

  async getToplookupCodes() {
    this.helper.showLoading();
    let self = this;
    let entitiesList = [];
    let topCodes = this.patientService.topLookupCodes.getValue();
    entitiesList = _.filter(topCodes, (eachEntitiy) => {
      return eachEntitiy.codeType == self.codeTypes[self.type]
    });
    this.entitiesList = JSON.parse(JSON.stringify(entitiesList));
    this.getFavoriteCodes();
  }

  async getFavoriteCodes() {
    let favouriteCodes = _.clone(this.patientService.userFavouriteCodes.getValue());
    this.favoriteList = (favouriteCodes.reduce((obj, eachEntitiy) => {
      eachEntitiy = _.omit(eachEntitiy, '__typename');
      if (eachEntitiy.codeType == this.codeTypes[this.type]) {
        this.userFavEntities[eachEntitiy.code] = eachEntitiy;
        if (this.codeTypes[this.type] == eachEntitiy.codeType) {
          let index = _.findIndex(this.entitiesList, { code: eachEntitiy.code });
          if (index == -1) {
            this.entitiesList.push(eachEntitiy);
          }
          obj[eachEntitiy.code] = !!JSON.parse(eachEntitiy.isFav);
        }
      }
      return obj;
    }, {}));
    this.existingUserFavourites = JSON.parse(JSON.stringify(this.userFavEntities));
    this.sortEntities();
    this.helper.hideLoading();
  }

  sortEntities() {
    this.entitiesList = _.sortBy(this.entitiesList, ((each: any) => {
      let isSelected = '';
      if (this.selectedEntities.length) {
        isSelected = _.findIndex(this.selectedEntities, { code: each.code, name: each.name }) > -1 ? 'A_' : 'B_';
      }
      let isFavorite = this.favoriteList[each.code] ? 'A_' : 'B_';
      return [isSelected, isFavorite, each.name].join('_');
    }));

  }

  lookupAllEntities() {
    let lookupModal = this.modalCtrl.create(EntityLookupPage, { type: this.type });
    lookupModal.present();
    lookupModal.onDidDismiss((lookupEntities: any) => {
      let self = this;
      if (lookupEntities && lookupEntities.length) {
        _.each(lookupEntities, function(entity: any) {
          _.findIndex(self.entitiesList, { code: entity.code }) == -1 ? self.entitiesList.push(entity) : '';
          _.findIndex(self.selectedEntities, { code: entity.code }) == -1 ? self.selectedEntities.push(entity) : '';
          self.userFavEntities[entity.code] = _.clone({ code: entity.code, codeType: self.codeTypes[self.type], name: entity.name, isFav: "false" });
          self.favoriteList[entity.code] = false;
        });
        this.sortEntities();
      }
    })
  }

  isEntitySelected(entity: any) {
    return _.findIndex(this.selectedEntities, { code: entity.code }) != -1;
  }

  isValid() {
    if (this.type == 'notes') {
      return _.isEmpty(this.notes.title);
    }
    else {
      return !this.selectedEntities.length;
    }
  }

  isEntityBilled(entity: any) {
    let index = _.findIndex(this.selectedEntities, { name: entity.name, code: entity.code });
    if (index > -1) {
      return this.selectedEntities[index].billStatus == 'Billed' ? true : false;
    }
    return false;
  }

  toggleSelection(item: any) {
    let index = _.findIndex(this.selectedEntities, { name: item.name, code: item.code });
    let entityObj: any = { name: item.name, code: item.code, billStatus: "UnBilled", date: moment(this.date).format("YYYYMMDD") };
    index == -1 ? this.selectedEntities.push(entityObj) : this.selectedEntities.splice(index, 1);
  }

  async toggleFavorite(item: any) {
    let index = _.findIndex(this.entitiesList, item);
    let lookupCodeObj = _.clone(this.entitiesList[index]);
    lookupCodeObj.isFav = !this.favoriteList[item.code];
    this.favoriteList[item.code] = lookupCodeObj.isFav;
    this.userFavEntities[item.code] = _.clone({ code: lookupCodeObj.code, codeType: this.codeTypes[this.type], name: lookupCodeObj.name, isFav: "" + lookupCodeObj.isFav });
  }

  async save() {
    try{
      if (this.type == 'notes') {
        let notes = _.clone(this.notes), result;
        notes.patientId = this.patientId;
        notes.date = moment(this.date).format('MM/DD/YYYY');
        this.helper.showLoading("Saving note");
        result = await this.patientService.upsertPatientNotes(notes, this.mode)
        this.helper.hideLoading();
        this.helper.showMessage("Note saved successfully.");
        this.notesCallback(_.values(result)[0]);
        this.view.dismiss();
      }
      else {
        this.view.dismiss(this.selectedEntities);
      }
    }catch(err){
      this.helper.hideLoading();
      console.log(err);
    }
  }

  async updateFavouriteEntities() {
    try {
      if (!_.isEqual(this.existingUserFavourites, this.userFavEntities)) {
        let userFavEntities = _.values(this.userFavEntities);
        if (userFavEntities.length) {
          await this.patientService.addGlobalCodesToFavList({ favs: userFavEntities });
          this.helper.getFavouriteCodes();
        }
      }
    } catch (err) {
      console.log(err);
    }
  }

  ionViewWillLeave() {
    let backDrop: any = document.getElementsByTagName('ion-picker-cmp');
    if (backDrop.length > 0) {
      for (let i = 0; i < backDrop.length; i++) {
        backDrop[i].style.display = 'none';
      }
    }
  }

  ngOnDestroy() {
    this.updateFavouriteEntities();
    this.helper.favEntitiesQueryRef ? this.helper.favEntitiesQueryRef.unsubscribe() : '';
  }
}
