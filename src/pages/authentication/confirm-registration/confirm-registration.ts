import { Component, OnDestroy, OnInit } from "@angular/core";
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { HomePage } from "../../home/home";
import { StorageService } from '../../../providers/storage-service';
import { LoginComponent } from '../login/login';
import { Helper } from '../../../providers/helper';
import { Auth } from 'aws-amplify';


@Component({
  selector: 'confirm-registration',
  templateUrl: './confirm-registration.html'
})

export class RegistrationConfirmationComponent implements OnInit {
  confirmationCode: string;
  email: string;
  private sub: any;

  constructor(
    public helper: Helper,
    public navCtrl: NavController,
    public params: NavParams,
    public storageService: StorageService,
    public viewCtrl: ViewController,
  ) { }

  ngOnInit() {
    this.email = this.params.get('email');
  }

  onConfirmRegistration() {
    this.helper.showLoading();
    Auth.confirmSignUp(this.email, this.confirmationCode)
      .then((response: any) => {``
        this.helper.hideLoading();
        this.helper.showMessage('User creation success, login to continue', 3000, 'toast-success');
        this.navCtrl.push(LoginComponent, { email: this.email });
      })
      .catch(err => {
        this.helper.hideLoading();
        this.helper.showMessage(err.message);
        console.log('err occured ', err)
      });
  }

}
