import { Component, OnDestroy, OnInit } from "@angular/core";
import { Events, MenuController, NavController, NavParams, Platform, ViewController } from 'ionic-angular';
import { ForgotPasswordStepComponent } from "../forgot-password/forgot-password";
import { Keyboard } from '@ionic-native/keyboard';
import { RegisterComponent } from "../register/register";
import { RegistrationConfirmationComponent } from "../confirm-registration/confirm-registration";
import { HomePage } from '../../home/home';
import { WelcomePage } from '../../welcome/welcome';
import { Helper } from '../../../providers/helper';
import { SubscriptionHelperSerivce, SubscriptionListing } from '../../../subscriptions-module';
import { StorageService } from '../../../providers/storage-service';
import { HospitalService } from '../../../providers/hospitalService';
import { Auth } from 'aws-amplify';
import { GraphqlService } from '../../../app/graphql.service';

const keyboard = window['Keyboard'];

@Component({
  selector: 'page-login',
  templateUrl: './login.html'
})
export class LoginComponent implements OnInit, OnDestroy {

  registerPage: any = RegisterComponent;
  forgotPasswordPage: any = ForgotPasswordStepComponent;
  email: string;
  password: string;
  errorMessage: string;
  mfaStep = false;
  mfaData = {
    destination: '',
    callback: null
  };
  showFooter: boolean = true;

  constructor(
    public helper: Helper,
    public hospitalService: HospitalService,
    public events: Events,
    public graphqlService: GraphqlService,
    // private keyboard: Keyboard,
    public navParams: NavParams,
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    public menuCtrl: MenuController,
    public platform: Platform,
    public storageService: StorageService,
    public subscriptionHelper: SubscriptionHelperSerivce
  ) {
    this.menuCtrl.swipeEnable(false);
    this.menuCtrl.enable(false);
    if (keyboard && (platform.is('android') || platform.is('ios'))) {
      keyboard.hideKeyboardAccessoryBar(true);
      keyboard.disableScroll(false);
    }

    if (this.navParams.get('email')) {
      this.email = this.navParams.get('email');
    }

    window.addEventListener('keyboardWillShow', (event) => {
      this.showFooter = false;
    });

    window.addEventListener('keyboardWillHide', () => {
      this.showFooter = true;
    });

  }

  ngOnInit() {
    this.errorMessage = null;
  }

  onLogin() {
    let self: any = this;
    this.errorMessage = null;
    this.helper.showLoading();
    let tempUser: any = {};
    Auth.signIn(this.email, this.password)
      .then((resposne: any) => {
        console.log('user logged in successfully', resposne);
        resposne.signInUserSession.idToken.payload
        tempUser.name = resposne.signInUserSession.idToken.payload.nickname;
        tempUser.email = resposne.username;
        tempUser.uid = resposne.signInUserSession.idToken.payload.sub;
        tempUser.aliasId = resposne.signInUserSession.idToken.payload['custom:aliasId'];
        tempUser.aliasName = resposne.signInUserSession.idToken.payload['custom:aliasName'];
        tempUser.groups = resposne.signInUserSession.idToken.payload['cognito:groups'];
        tempUser.userRole = resposne.signInUserSession.idToken.payload['custom:userRole'];
        return this.graphqlService.hydrateClient();
      })
      .then(async () => {
        if (tempUser.userRole == 'Doctor') {
          return this.storageService.getRolesData({ userType: tempUser.userRole || 'Doctor' })
        } else {
          try {
            let response: any = await this.hospitalService.listMyRelations();
            console.log('response of connected doctors', response);
            let doctors = response;
            if (!(doctors || []).length) {
              return this.storageService.getRolesData({ userType: "Default:" + tempUser.userRole })
            } else {
              let currentDoctor = await this.storageService.getSettings('CURRENT_DOCTOR')
              currentDoctor = currentDoctor && currentDoctor.connectedUserAlias ? currentDoctor : doctors[0];
              return this.hospitalService.getUserRoleSettings(currentDoctor.connectedUserAlias);
            }
          } catch (err) {
            this.helper.hideLoading();
            this.helper.showMessage("Failed to login, please try again", 3000);
          }
        }
      })
      .then((roles: any) => {
        if (roles && roles.roleList) {
          roles = { connectedUserAlias: roles.connectedUserAlias, supportedRoles: (roles.roleList || []) }
        }
        console.log('get the roles', roles)
        tempUser.userRoles = ((roles && (roles.supportedRoles || []).length) ? roles.supportedRoles : []);
        this.helper.permissionObservable.next(tempUser.userRoles)
        console.log('this.helper', this.helper)
        return this.helper.login(tempUser);
      })
      .then(() => {
        this.events.publish('user:login');
        this.helper.hideLoading();
        this.navigateUser();
      })
      .catch(err => {
        console.log('err occured', err)
        if (err.message == 'User is not confirmed.') {
          Auth.resendSignUp(this.email)
            .then((response: any) => {
              self.helper.hideLoading();
              self.navCtrl.push(RegistrationConfirmationComponent, { email: self.email });
            })
            .catch((err) => {
              console.log('send code error', err);
              self.helper.hideLoading();
              self.helper.showMessage(err.message, 3000);
            });
        }
        else {
          this.helper.hideLoading();
          this.helper.showMessage(err.message, 3000);
        }
      })
  }

  navigateUser() {
    let self: any = this;
    this.helper.storageService.getSettings('APP_READY')
      .then((value: any) => {
        if (value == "true") {
          this.helper.fetchUserCloudSettings()
            .then(() => {
              return self.storageService.getSettings('STRIPE_CUST_ID');
            })
            .then((stripeCustId: any) => {
              if (this.helper.hasPermission('SUBSCRIBE_SUBSCRIPTION')) {
                if (stripeCustId && stripeCustId != 'null') {
                  self.subscriptionHelper.isSubscriptionActive(stripeCustId)
                    .then((subscriptionResponse: any) => {
                      if (subscriptionResponse.isExists) {
                        this.onLoginSuccess();
                      }
                      else {
                        self.helper.hideLoading();
                        self.navCtrl.setRoot(SubscriptionListing);
                      }
                    })
                } else {
                  self.helper.hideLoading();
                  self.navCtrl.setRoot(SubscriptionListing);
                }
              } else {
                this.onLoginSuccess();
              }
            })
        } else {
          self.helper.hideLoading();
          self.navCtrl.setRoot(WelcomePage);
        }
      })
  }

  onLoginSuccess() {
    this.helper.hideLoading();
    this.helper.showMessage('Logged-in successfully');
    this.helper.getTopLookupCodes();
    this.navCtrl.setRoot(HomePage);
  }

  ngOnDestroy() {
    this.menuCtrl.swipeEnable(true);
    this.menuCtrl.enable(true);
  }

}
