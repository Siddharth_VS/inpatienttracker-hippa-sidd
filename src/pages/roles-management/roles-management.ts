import { Component } from '@angular/core';
import { AlertController, LoadingController, ModalController, NavController, NavParams, ViewController } from 'ionic-angular';
import * as _ from 'underscore';
import { Helper } from '../../providers/helper';
import { StorageService } from '../../providers/storage-service';
import { HospitalService } from '../../providers/hospitalService';

@Component({
  selector: 'page-roles-management',
  templateUrl: 'roles-management.html',
})
export class RolesManagementPage {
  permissions: any[];
  roles: any = [];
  selectedPermissions: any = [];
  selectedRoleId: any;
  roleName: string;
  _ = _;
  supportedRoles: any[] = [];
  title: string;
  isModal: boolean = false
  connectedUserAlias: any = '';
  isEdit: boolean = false;
  userType: string;
  relation: any = {};

  constructor(
    public alertCtrl: AlertController,
    public helper: Helper,
    public hospitalService: HospitalService,
    public navCtrl: NavController,
    public navParams: NavParams,
    public storageService: StorageService,
    public viewCtrl: ViewController
  ) {
    this.title = this.navParams.get('title') || 'Roles Management'
    this.connectedUserAlias = this.navParams.get('connectedUserAlias') || ''
    this.isModal = this.navParams.get('isModal') || false
    this.isEdit = this.navParams.get('isEdit') || false
    this.userType = this.navParams.get('userType') || ''
    this.relation = this.navParams.get('relation') || {}
    this.helper.showLoading();
    this.getpermissions()
    this.storageService.listRoles()
      .then(async (response: any) => {
        console.log('listRoles', response)
        if ((response).length) {
          this.roles = _.clone(response);
          if (this.userType && !this.isEdit) {
            this.selectedRoleId = this.userType;
            let index = _.findIndex(this.roles, { userType: this.userType })
            index > -1 && this.roles[index] ? this.selectedPermissions = this.supportedRoles = this.roles[index].supportedRoles : ''
          } else if (this.userType && this.isEdit) {
            this.selectedRoleId = this.userType;
            try {
              let response: any = await this.hospitalService.getUserRoleSettingsForRelation(this.relation.connectedUserAliasId);
              (response || {}).roleList && response.roleList.length ? this.selectedPermissions = this.supportedRoles = response.roleList : ''
            } catch (err) {
              this.helper.hideLoading();
              console.log('err occured', err)
            }
          } else {
            this.selectedRoleId = (this.roles)[0].userType;
            this.supportedRoles = (this.roles)[0].supportedRoles;
          }
        }
        this.helper.hideLoading();
      })
      .catch((err) => {
        this.helper.hideLoading();
        console.log('err occured', err)
      })

  }

  getpermissions() {
    this.storageService.listPermissions()
      .then((response: any) => {
        if (this.userType) {
          let temp = [];
          _.each(response, (ele) => {
            let values = _.filter(ele.valueList, { doctorControlled: "true" })
            if (values.length) {
              temp.push({ ...ele, ...{ valueList: values } })
            }
          })
          temp = _.sortBy(temp, function(ele) { return ele.sortOrder; });
          this.permissions = temp;
        } else {
          this.permissions = _.sortBy(response, function(ele) { return ele.sortOrder });
        }
      })
      .catch((err) => {
        console.log('err occured', err)
      })
  }

  save() {
    // this.helper.showLoading();
    if ((this.relation || {}).connectedUserAliasId && !this.isEdit) {
      this.createRelations()
    } else if ((this.relation || {}).connectedUserAliasId && this.isEdit) {
      this.updatePermissions()
    }
  }

  async createPermissions() {
    try {
      this.helper.showLoading();
      await this.hospitalService.createUserRoleSettings(this.selectedPermissions, this.relation.connectedUserAliasId)
      this.helper.hideLoading();
      this.helper.showMessage('Permissions have been added successfully');
    } catch (err) {
      console.log('err occured', err)
      this.helper.hideLoading();
      this.helper.showMessage('Failed to add the permissions, please try again');
    }
  }

  togglePermissionGroup(permissionGroup, index) {
    console.log('this.permissions', this.permissions)
    console.log('index', index)
    console.log('this.permissions[index].isExpanded', this.permissions[index].isExpanded)
    this.permissions[index].isExpanded = !!!this.permissions[index].isExpanded
    // return !!!permissionGroup.isExpanded
  }

  async updatePermissions() {
    try {
      this.helper.showLoading();
      await this.hospitalService.updateUserRoleSettings(this.selectedPermissions, this.relation.connectedUserAliasId)
      this.viewCtrl.dismiss();
      this.helper.hideLoading();
      this.helper.showMessage('Permissions have been updated successfully');
    } catch (err) {
      console.log('err occured', err)
      this.helper.hideLoading();
      this.helper.showMessage('Failed to update the permissions, please try again');

    }
  }

  onSelectChange(data: any) {
    console.log('onSelectChange data:', data)
  }

  selectPermissionByAdmin(permission: any) {
    console.log('selectPermissionByAdmin permission:', permission)
  }

  async createRelations() {
    try {
      this.helper.showLoading();
      await this.hospitalService.createRelations({
        connectedUserAliasId: this.relation.connectedUserAliasId,
        hospitalId: this.relation.hospitalId
      })
      await this.hospitalService.createUserRoleSettings(
        this.selectedPermissions,
        this.relation.connectedUserAliasId
      )
      this.navCtrl.remove(0, this.navCtrl.getViews().length);
      this.helper.hideLoading();
      this.helper.showMessage('Successfully added ' + this.userType);
    }
    catch (err) {
      this.helper.hideLoading();
      this.helper.showMessage('Failed to create relation');
      console.log('err occured', err)
    }
  }

  selectPermission(permission: any) {
    if (!this.selectedPermissions) {
      this.selectedPermissions = [];
    }
    if (this.selectedPermissions.includes(permission.fieldID)) {
      let index = this.selectedPermissions.indexOf(permission.fieldID);
      this.selectedPermissions.splice(index, 1);
    } else {
      this.selectedPermissions.push(permission.fieldID);
    }
  }

  changeDoctorControlledStatus(data: any) {
    this.helper.showLoading();
    console.log('changeDoctorControlledStatus data:', data)
    this.helper.hideLoading();
  }
}
