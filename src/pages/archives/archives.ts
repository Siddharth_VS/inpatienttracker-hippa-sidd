import { Component, ViewChild } from '@angular/core';
import { AlertController, NavController, ModalController, Select } from 'ionic-angular';
import { BilledRecordPage } from './billed-record/billed-record';
import { AddPatientPage } from '../add-patient/add-patient';

import { StorageService } from '../../providers/storage-service';
import { PatientService } from '../../providers/patient-service';
import { HospitalService } from '../../providers/hospitalService';
import { Helper } from '../../providers/helper';
import * as _ from 'underscore';

@Component({
  selector: 'page-archive',
  templateUrl: 'archives.html'
})

export class ArchivePage {
  @ViewChild('allHospitals') hospitalSelect: Select;
  uid: any;
  selectOptions = {
    title: "Switch Hospital"
  };
  currentHospital: any = {};
  archivePatients: any[] = [];
  patientDetails: any[] = [];
  hospitals: any;
  query = '';
  searchKeys = ['name', 'medicalRecordNumber', 'room'];
  isVisible: boolean = false;
  archivedQueryRef: any;

  constructor(
    public alertCtrl: AlertController,
    public helper: Helper,
    public modal: ModalController,
    public navCtrl: NavController,
    public patientService: PatientService,
    public storageService: StorageService,
    private hospitalService: HospitalService,
  ) {
    this.getUserHospitals();
    this.init();
  }

  readmit(patient:any){
    let patientData = (({ name, dateOfBirth, hospitalId, medicalRecordNumber,referringDoctor,sex }) => ({ name, dateOfBirth, hospitalId, medicalRecordNumber,referringDoctor,sex }))(patient);
            this.navCtrl.push(AddPatientPage, {patient:patientData, hospitalId: patient.hospitalId});
  }

  cancelBilling(patientData:any) {
    let alert = this.alertCtrl.create({
      title: 'Confirm Cancellation',
      message: 'Are you sure to cancel this record from Billing?',
      buttons: [{
        text: 'NO',
        role: 'cancel'
      }, {
        text: 'YES',
        handler: () => {
          let patient: any = Object.assign({}, patientData);
          patient.status = 'ACTIVE';
          this.patientService.upsertPatient(patient)
            .then(() => {
              this.helper.showMessage('Record successfully got cancelled', 3000);
              this.updateArchives(patient.patientId, "DELETE");

            });
        }
      }]
    })
    alert.present();
  }

  getUserHospitals() {
    let self = this;
    this.hospitalService.getUserHospitals()
      .then((hospitals: any) => {
        this.hospitals = [];
        _.each(hospitals, function(hospital: any) {
          if (hospital.hospitalId && hospital.isRemoved != 'true' && hospital.approvalStatus == 'approved') {
            self.hospitals.push(hospital);
          }
        })
        if (_.isEmpty(this.currentHospital) && self.hospitals.length) {
          let i = _.findIndex(this.hospitals, { hospitalId: this.currentHospital.hospitalId });
          if (i > -1)
            this.currentHospital = { ...this.hospitals[i].hospitalDetail };
        }
      })
  }

  changeHospital(hospitalId: any) {
    this.isVisible = false;
    this.query = '';
    let index = _.findIndex(this.hospitals, { hospitalId: hospitalId });
    this.currentHospital = { ...this.hospitals[index].hospitalDetail, ...{ hospitalId: hospitalId } };
    this.archivePatients = [];
    this.init();
  }

  selectHospital() {
    this.hospitalSelect.open();
  }

  async init() {
    try {
      this.helper.showLoading();
      this.currentHospital = _.isEmpty(this.currentHospital) ? await this.storageService.getSettings('HOSPITAL_DATA') : this.currentHospital;
      if (!_.isEmpty(this.currentHospital)) {
        await this.loadArchivePatients();
      }
      this.helper.hideLoading();
    } catch (err) {
      this.helper.hideLoading();
      console.log("error", err)
    }
  }

  loadArchivePatients() {
    return new Promise((resolve) => {
      this.patientService.getArchivedPatients(this.archivedQueryRef, this.currentHospital.hospitalId)
        .then((patientsSubscription: any) => {
          patientsSubscription
            .subscribe((data: any) => {
              let patients = data.queryArchievedPatientDetails.items;
              data.queryArchievedPatientDetails.items
              let self = this;
              self.archivePatients = [];
              patients.forEach((patient: any) => {
                self.archivePatients.push(patient);
              })
              self.archivePatients = _.sortBy(self.archivePatients,
                (patient: any) => {
                  return [patient.dischargeDate];
                })
              self.archivePatients = this.archivePatients.reverse();
              resolve();
            })
        })
    })
  }

  billRecords(patient: any) {
    let recordModal = this.modal.create(BilledRecordPage, {
      patient: patient, hospital: this.currentHospital,
      uid: this.uid,
      updateArchivePageCallback: (patientObj: any, type: any) => { this.updateArchives(patientObj, type) }

    });
    recordModal.present();
  }

  toggleSearchBar() {
    this.query = "";
    this.isVisible = !this.isVisible;
  }

  updateArchives(patientId: any, type: any) {
    if (type == "DELETE") {
      let index = _.findIndex(this.archivePatients, { patientId: patientId });
      index > -1 ? this.archivePatients.splice(index, 1) : '';
    }
  }
}
