import { Component, ViewChild } from '@angular/core';
import { AlertController, ActionSheetController, Content, NavController, ModalController, NavParams, Platform, Select } from 'ionic-angular';
import { HospitalService } from '../../providers/hospitalService';
import { Helper } from '../../providers/helper';
import { PatientService } from '../../providers/patient-service';
import { PatientDetailPage } from '../patient-details/patient-details';
import { Observable } from 'rxjs/Observable';
import { AddPatientPage } from '../add-patient/add-patient';
import { StorageService } from '../../providers/storage-service';
import { HospitalList } from '../settings/hospital/hospital-list/hospital-list';
import { ManageCard } from '../settings/cards-management/manage-card/manage-card';
import { HospitalCardsListing } from '../settings/cards-management/cards-listing/cards-listing';

import { Camera, CameraOptions } from "@ionic-native/camera";
import { Storage } from '@ionic/storage';
import { File } from '@ionic-native/file';

import * as moment from 'moment';
import * as _ from 'underscore';
declare const ImageCapturePlugin: any;
declare const cordova: any;


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {

  @ViewChild(Content) content: Content;
  @ViewChild(Select) hospitalSelect: Select;
  @ViewChild('doctorList') doctorListSelect: Select;
  query = "";
  patients: any = [];
  selectOptions = {
    title: "Switch Hospital"
  };
  selectDoctorListOptions = {
    title: "Switch Doctor"
  };
  sortKey: any = "name";
  moment = moment;
  sortObject: any = { name: "NAME", medicalRecordNumber: "FIN NO", room: "ROOM NO" };
  searchKeys = ["name", "medicalRecordNumber"];
  showSearchBar: boolean = false;
  hospitalId: string;
  patientInfoQueryRef: any;
  hospitals: any = [];
  currentHospital: any = { hospitalId: null };
  selectedPatientsCount: number = 0;
  card: any = {};
  //camera configuration
  options: CameraOptions = {
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE,
    correctOrientation: true,
    quality: 100,
    allowEdit: true,
    destinationType: 0,
  };
  base64data: any;
  currentDoctor: any = {};
  doctors: any[] = [];

  constructor(
    public actionSheetCtrl: ActionSheetController,
    public alertCtrl: AlertController,
    public camera: Camera,
    public file: File,
    public helper: Helper,
    private hospitalService: HospitalService,
    public modal: ModalController,
    public navCtrl: NavController,
    public navParams: NavParams,
    private patientService: PatientService,
    private platform: Platform,
    public storage: Storage,
    public storageService: StorageService
  ) {
  }

  ionViewDidLoad() {
    this.init();
  }

  abbyyRtrSdkPluginCallback(result) {
    console.log(result);
  }

  textCapture() {
    AbbyyRtrSdk.startTextCapture(this.abbyyRtrSdkPluginCallback, {
      selectableRecognitionLanguages: ["English", "French", "German", "Italian", "Polish", "PortugueseBrazilian",
        "Russian", "ChineseSimplified", "ChineseTraditional", "Japanese", "Korean", "Spanish"],
      recognitionLanguages: ["English"],

      licenseFileName: "AbbyyRtrSdk.license",
      isFlashlightVisible: true,
      stopWhenStable: true,
      areaOfInterest: ("0.9 0.38"),
      // areaOfInterest : (areaOfInterestWidth.current() + " " + areaOfInterestHeight.current()),
      isStopButtonVisible: true,
    });
  }

  getUserHospitals() {
    return new Promise(async (resolve, reject) => {
      let self = this;
      try {
        self.hospitals = [];
        self.currentHospital = (await this.storageService.getSettings("HOSPITAL_DATA")) || {};
        this.hospitalId = !_.isEmpty(this.currentHospital) ? this.currentHospital.hospitalId : (await this.storageService.getSettings("HOSPITAL_ID") || null);
        let hospitals: any = await this.hospitalService.getUserHospitals();
        await self.hospitalService.autoSelectHosptial()
        if (hospitals && hospitals.length) {
          _.each(hospitals, function(hospital: any) {
            if (hospital.isRemoved != 'true' && ((hospital.isCustom == true && hospital.approvalStatus == 'approved') || hospital.isCustom != "true")) {
              self.hospitals.push(hospital);
            }
          })
          let i = _.findIndex(this.hospitals, { hospitalId: this.hospitalId });
          if (i == -1 && hospitals.length == 1 && hospitals[0].hospitalId != this.hospitalId) {
            i = 0;
            this.hospitalId = this.hospitals[i].hospitalId;
            this.currentHospital = { ...this.hospitals[i].hospitalDetail, ...{ hospitalId: this.hospitalId } };
            this.changeHospital(this.currentHospital.hospitalId);
          }
          else {
            i = i > -1 ? i : 0;
            this.currentHospital = { ...this.hospitals[i].hospitalDetail, ...{ hospitalId: this.hospitalId } };
          }
          resolve();
        }
        else {
          this.currentHospital = {};
          this.storage.remove("HOSPITAL_DATA");
          this.storage.remove("HOSPITAL_ID");
          resolve();
        }
      } catch (err) {
        console.log(err);
        reject(err)
      }
    })
  }

  showNoHospitalAlert() {
    let alert = this.alertCtrl.create({
      title: 'Add new hospital',
      message: 'No hospitals found. Would you like to add a hospital?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Add',
          handler: () => {
            this.openHospitalList();
          }
        }
      ]
    });
    alert.present();
  }

  openHospitalList() {
    let hospitalListModal = this.modal.create(HospitalList);
    hospitalListModal.onDidDismiss((hospital) => {
      this.getUserHospitals();
    })
    hospitalListModal.present();
  }

  async init() {
    try {
      let self = this;
      this.helper.showLoading();
      await this.getUserHospitals();
      if (this.hospitalId) {
        let patientInfo: any;
        if (this.helper.hasPermission('SWITCH_DOCTOR')) {
          let response: any = await this.hospitalService.listMyRelations();
          console.log('response of connected doctors', response);
          this.doctors = response;
          if ((this.doctors || []).length) {
            let currentDoctor = await this.storageService.getSettings('CURRENT_DOCTOR');
            this.currentDoctor = currentDoctor && currentDoctor.connectedUserAlias ? currentDoctor : this.doctors[0];
            await this.storageService.setLocalSettings('CURRENT_DOCTOR', this.currentDoctor)
            let response: any = await this.hospitalService.getUserRoleSettings(this.currentDoctor.connectedUserAlias);
            patientInfo = await this.patientService.getActivePatientForDr(this.patientInfoQueryRef, this.hospitalId, this.currentDoctor.connectedUserAlias);
            let permissions:any
            permissions = (response || {}).roleList && response.roleList.length ? response.roleList : [];
            this.helper.setPermissionData(permissions)
          }
        } else {
          patientInfo = await this.patientService.getActivePatients(this.patientInfoQueryRef, this.hospitalId);
        }
        if (patientInfo) {
          patientInfo
            .take(1)
            .subscribe(async (res: any) => {
              res = JSON.parse(JSON.stringify(this.helper.hasPermission('SWITCH_DOCTOR') ? res.queryActivePatientDetailsForDr.items : res.queryActivePatientDetails.items));
              this.patients = res;
              this.sortKey = await this.storage.get('SORT_KEY') || this.sortKey;
              this.selectSortByOptions(this.sortKey);
              _.each(res, (eachPatient) => {
                self.selectedPatientsCount += self.getItemClass(eachPatient) == 'selected' ? 1 : 0;
              })
              this.helper.hideLoading();
            })
        } else {
          this.helper.hideLoading();
        }
      }
      else {
        this.helper.hideLoading();
      }

      // else{
      //   let response:any = await this.hospitalService.listMyRelations()
      //   console.log('response of connected doctors',response)
      //   this.doctors = response;
      //   if((this.doctors || []).length){
      //     this.currentDoctor = await this.storageService.getSettings('CURRENT_DOCTOR') || this.doctors[0]
      //     await this.storageService.setLocalSettings('CURRENT_DOCTOR', this.currentDoctor)
      //   }
      //   this.helper.hideLoading();
      // }
    } catch (err) {
      this.helper.hideLoading();
      console.log(err);
    }
  }

  async changeHospital(hospitalId: any, refreshView?: boolean) {
    try {
      let index = _.findIndex(this.hospitals, { hospitalId: hospitalId })
      index = index > -1 ? index : 0;
      let selectedHospital = { ...this.hospitals[index].hospitalDetail, ...{ hospitalId: hospitalId } };
      this.currentHospital = selectedHospital;
      this.hospitalId = selectedHospital.hospitalId;
      this.selectedPatientsCount = 0;
      this.patients = [];
      await this.storageService.setSettings('HOSPITAL_ID', hospitalId);
      await this.storage.set("HOSPITAL_DATA", selectedHospital);
      this.init();
    } catch (err) {
      console.log(err);
    }
  }

  toggleSearchBar() {
    this.query = "";
    this.showSearchBar = !this.showSearchBar;
  }

  viewPatientDetails(patient: any) {
    this.showSearchBar = false;
    this.navCtrl.push(PatientDetailPage, {
      patient: patient,
      hospitalId: this.hospitalId,
      updateHomepageCallback: (patientObj, type) => { this.updatePatients(patientObj, type) }
    });
    this.query = '';
  }

  updatePatients(patientObj, type) {
    console.log(this.patients);
    let index = _.findIndex(this.patients, { patientId: patientObj.patientId });
    if (index > -1 && type == "DELETE") {
      index > -1 ? this.patients.splice(index, 1) : '';
    }
    else if (index > -1) {
      this.patients[index] = patientObj;
    }
  }

  getItemClass(patient: any) {
    return patient.lastUpdated && +moment().startOf("day").format("x") <= +moment(+patient.lastUpdated).startOf("day").format("x") ? "selected" : "";
  }

  selectHospital() {
    this.query = '';
    this.showSearchBar = false;
    if (!this.hospitals.length) {
      this.showNoHospitalAlert();
    } else {
      this.hospitalSelect.open();
    }
  }

  selectDoctor() {
    if(this.currentDoctor && this.currentDoctor.connectedUserAliasName){
      this.doctorListSelect.open();
    }
  }

  async changeDoctor(connectedUserAlias: string) {
    console.log('data', connectedUserAlias)
    let index = _.findIndex(this.doctors, { connectedUserAlias });
    if (index > -1) {
      await this.storageService.setLocalSettings('CURRENT_DOCTOR', this.doctors[index])
      this.init()
    }
  }

  ngOnDestroy() {
    this.patientInfoQueryRef ? this.patientInfoQueryRef.unsubscribe() : '';
  }

  markVisitedPatient(patientId: string, lastUpdated: string, hospitalId: string) {
    this.helper.showLoading();
    this.patientService.markVisitedPatient(patientId, lastUpdated, hospitalId)
      .then((res: any) => {
        res = _.values(res)[0];
        let index = _.findIndex(this.patients, { patientId: res.patientId })
        this.patients[index].lastUpdated = res.lastUpdated;
        this.selectedPatientsCount += this.getItemClass(this.patients[index]) == 'selected' ? 1 : -1;
        this.helper.hideLoading();
      });
  }

  openPatientForm(data?: any) {
    this.navCtrl.push(ManageCard, { hospitalId:this.currentHospital.hospitalId, currentHospital: this.currentHospital, cardMappingType: 'patient_creation_manual', showBackButton: true })
  }

  addPatient() {
    let buttons: any = [];
    if (this.helper.hasPermission('CREATE_PATIENT_MANUAL', this.helper.permissionObservable.getValue())) {
      buttons.push({
        text: "Manual",
        handler: () => {
          this.openPatientForm();
        }
      })
    }
    if (this.helper.hasPermission('CREATE_PATIENT_SCAN', this.helper.permissionObservable.getValue())) {
      buttons.push(
        {
          text: "Scan",
          handler: () => {
            // this.scanCard();
            // this.scanCardTemp();
            this.openCardListing();
          }
        })
    }
    let alert = this.alertCtrl.create({
      title: "Add Patient",
      message: "Select method to add patient",
      cssClass: "add-patient-alert",
      buttons: buttons
    });
    alert.present();
  }

  selectFilterBy(myEvent: any) {
    console.log(this.sortKey);
    let actionSheet = this.actionSheetCtrl.create({
      title: "Sort By",
      subTitle: "Patients are sorted based on your selection",
      cssClass: "sort-options",
      buttons: [
        {
          text: "NAME",
          cssClass: this.sortKey == 'name' ? 'marked-option' : 'damn',
          handler: () => {
            this.selectSortByOptions("name");
          }
        },
        {
          text: "FIN NO",
          cssClass: this.sortKey == 'medicalRecordNumber' ? 'marked-option' : 'damn',
          handler: () => {
            this.selectSortByOptions("medicalRecordNumber");
          }
        },
        {
          text: "ROOM NO",
          cssClass: this.sortKey == 'room' ? 'marked-option' : 'damn',
          handler: () => {
            this.selectSortByOptions("room");
          }
        },
        {
          text: "Cancel",
          role: "cancel"
        }
      ]
    });
    actionSheet.present();
  }

  selectSortByOptions(sortKey: any) {
    this.storageService.setLocalSettings("SORT_KEY", sortKey);
    this.sortKey = sortKey;
    this.patients = this.sortPatients(this.patients);
  }

  sortPatients(patients: any) {
    let self = this,
      temp: any = [];
    temp = _.sortBy(patients, (patient: any) => {
      return patient[self.sortKey];
    });
    return temp;
  }

  setLastUpdatedStatus(patient: any, event: any, index: number) {
    if (!this.helper.hasPermission('SWITCH_DOCTOR')) {
      if (!event.checked) {
        patient.lastUpdated = moment().subtract(1, 'day').format('x');
        this.markVisitedPatient(patient.patientId, patient.lastUpdated, patient.hospitalId);
      } else {
        patient.lastUpdated = moment().format('x');
        this.markVisitedPatient(patient.patientId, patient.lastUpdated, patient.hospitalId);
      }
    }
  }

  scanCardTemp(){
    // this.navCtrl.push(ManageCard, { rawImage: JSON.parse(localStorage.getItem('scanned-card')), image: localStorage.getItem('capturedBase64data'), currentHospital: this.currentHospital, cardMappingType: 'patient_creation_scan', showBackButton: true })
  }

  openCardListing(){
    this.navCtrl.push(HospitalCardsListing, { screenType: 'cardSelectionForScanning', currentHospital: this.currentHospital, showBackButton: true })
      // this.navCtrl.push(HospitalCardsListing, { rawImage: JSON.parse(localStorage.getItem('scanned-card')), image: localStorage.getItem('capturedBase64data'), currentHospital: this.currentHospital, cardMappingType: 'patient_creation_scan', showBackButton: true })
  }

  scanCard() {
    let self: any = this;
    if (this.currentHospital.code == 'CARD_NOT_SUPPORTED') {
      this.helper.basicAlert(this.currentHospital.name);
      return false;
    }
    new Promise((resolve, reject) => {
      if (this.platform.is('ios')) {
        ImageCapturePlugin.takePhoto([350, 160], function(data) {
          this.helper.showLoading();
          localStorage.setItem('data', data);
          resolve(data);
        }, (err) => {
          this.helper.hideLoading();
          console.log(err);
          reject(err);
        })
      }
      else {
        this.camera.getPicture(this.options)
          .then((base64Image: any) => {
            // imageData is either a base64 encoded string or a file URI
            // If it's base64:
            resolve(base64Image);
          })
          .catch((err) => {
            console.log(err);
            reject(err);
          });
      }
    })
      .then((base64Image: any) => {
        return this.helper
          .scanCard(base64Image, this.currentHospital.code);
      })
      .then((res: any) => {
        this.helper.hideLoading();
        this.card = res.card;
        if (this.card.error) {
          this.helper.showMessage('Failed to scan, please try again');
        }
        else {
          this.navCtrl.push(AddPatientPage, {
            patient: {
              name: this.card.name,
              medicalRecordNumber: this.card.medicalRecordNumber,
              sex: this.card.sex,
              referringDoctor: this.card.referringDoctor,
              dateOfAdmission: this.card.dateOfAdmission ? moment(new Date(this.card.dateOfAdmission)).format('') : '',
              room: "",
              accountNumber: this.card.acct ? this.card.acct : '',
              dateOfBirth: this.card.dateOfBirth ? moment(new Date(this.card.dateOfBirth)).format('') : ''
            },
            hospitalId: this.hospitalId
          });
        }
      })
      .catch((err) => {
        this.helper.hideLoading();
      });
  }

}
