import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { NavParams, ModalController, ToastController, ViewController } from 'ionic-angular';
import { StorageService } from '../../providers/storage-service';
import { PatientService } from '../../providers/patient-service';
import { FormControl } from '@angular/forms';
import { Helper } from '../../providers/helper';
import * as moment from 'moment';
import * as _ from 'underscore';
import 'rxjs/add/operator/debounceTime';

@Component({
  selector: 'page-entity-lookup',
  templateUrl: 'entity-lookup.html'
})

export class EntityLookupPage {
  type: string;
  query: any = '';
  date: any = moment().format();
  entitiesList: any = [];
  selectedEntities: any = [];
  codeTypes: any = {
    'procedures': 'CPT',
    'diagnosis': 'ICD',
    'em_codes': 'CPT',
  }
  searchControl: any = new FormControl();
  title: string;
  skipToken: string = "";
  globalCodesQueryRef: any;
  searchQueryRef: any;
  moment = moment;
  _ = _;

  constructor(
    public helper: Helper,
    public http: Http,
    public modalCtrl: ModalController,
    public params: NavParams,
    public storageService: StorageService,
    public patientService: PatientService,
    public toastCtrl: ToastController,
    public view: ViewController
  ) {
    this.type = this.codeTypes[this.params.get('type')];
    this.title = params.get('type').split('_').join(' ') + ' Lookup';
    this.loadInitialEntities('code');
  }

  loadInitialEntities(orderBy: string, searchQuery?: string) {
    this.getEntities(this.type, searchQuery || "")
      .then((entities: any) => {
        this.entitiesList = Object.assign([], entities);
        this.helper.hideLoading();
      });
  }

  ionViewDidLoad() {
    setTimeout(() => { this.helper.showLoading() }, 100)
    this.searchControl.valueChanges
      .debounceTime(2500)
      .subscribe((search: any) => {
        this.helper.showLoading();
        search ? this.entitiesList = [] : '';
        this.loadInitialEntities('code', search);
      });
  }

  loadMoreLookups(infiniteScroll: any) {
    let skip: any = this.entitiesList.length;
    let orderBy = 'code';
    this.getEntities(this.type, this.query || '')
      .then((loadedEntities: any) => {
        loadedEntities = Object.assign([], loadedEntities);
        this.entitiesList = [...this.entitiesList, ...loadedEntities];
        infiniteScroll.complete();
      })
  }

  trackByFn(index, item) {
    return index;
  }

  toggleSelection(entity: any) {
    let entityObj: any = { name: entity.name, code: entity.code, date: moment(this.date).format("YYYYMMDD"), billStatus: "UnBilled" };
    let index = _.findIndex(this.selectedEntities, entityObj);
    index == -1 ? this.selectedEntities.push(entityObj) : this.selectedEntities.splice(index, 1);
  }

  async save() {
    try {
      this.helper.showLoading("Saving lookup code");
      let savableEntities = [];
      _.each(this.selectedEntities, (eachEntity) => {
        savableEntities.push({ name: eachEntity.name, code: eachEntity.code, codeType: this.params.get('type') == "em_code" ? "VISIT" : this.type });
      })
      let result = await this.patientService.addGlobalCodesToFavList({ favs: savableEntities });
      this.dismiss(this.selectedEntities);
      this.helper.hideLoading();
    } catch (err) {
      this.helper.hideLoading();
      console.log(err);
    }
  }

  getEntities(type: string, searchKey) {
    return new Promise(async (resolve, reject) => {
      try{
        let variables = { from: this.entitiesList.length, search: searchKey };
        let res: any = await this.patientService.getGlobalCodes(type, this.globalCodesQueryRef, variables)
        res.take(1)
          .subscribe((codes) => {
            let entitiesObj = _.values(codes)[0] || [];
            resolve(entitiesObj);
          })
      }catch(err){
        console.log(err);
        reject();
      }
    })
  }

  dismiss(selectedEntities?: any) {
    this.view.dismiss(selectedEntities || null);
  }

  ngOnDestroy() {
    this.globalCodesQueryRef ? this.globalCodesQueryRef.unsubscribe() : '';
  }
}
