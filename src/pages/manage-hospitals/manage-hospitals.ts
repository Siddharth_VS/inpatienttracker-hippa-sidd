import { Component } from '@angular/core';
import { Helper } from '../../providers/helper';
import { HospitalService } from '../../providers/hospitalService';
import { AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
@Component({
  selector: 'manage-hospitals-page',
  templateUrl: 'manage-hospitals.html'
})

export class ManageHospitalsPage {
  cloudhospitals: any = [];
  localhospitals: any = [];
  segment: string = 'local';
  constructor(
    public alertCtrl: AlertController,
    private helper: Helper,
    private hospitalService: HospitalService,
    private storage: Storage
  ) {
    this.init();
  }

  init(){
    this.hospitalService.listAllCustomUserHospitals(100, '')
      .then((response: any) => {
        this.localhospitals = response.items;
        this.cloudhospitals = this.hospitalService.hospitals.getValue();
      })
      .catch((err) => {
        console.log('err occured', err)
      })
  }
  openRejectPrompt(hospital:any) {
    console.log('reject clicked',hospital);
    const prompt = this.alertCtrl.create({
      title: 'Are you sure want to reject Hospital?',
      message: "Enter reasons if any",
      inputs: [
        {
          name: 'Reason',
          placeholder: 'Reason'
        },
      ],
      buttons: [
        {
          text: 'No',
          role: 'cancel'
        },
        {
          text: 'Yes',
          handler: data => {
            this.rejectHospital(hospital);
          }
        }
      ]
    });
    prompt.present();
  }

  async approveHospital(hospitalData:any) {
    console.log('hospital',hospitalData)
    this.helper.showLoading();
    this.helper.showMessage('Hospital has been approved successfully', 3000, 'toast-success')
     let hospital = hospitalData.hospitalDetail;
     hospital.hospitalId = hospitalData.hospitalId;
    this.helper.sendEmail(hospitalData.createdByEmail, {operationType: 'approveHospital', userId:hospitalData.userId, hospital:hospital})
      .then((response:any) => {
        console.log('email sent successfully')
        let res = typeof(response._body) == 'string' ? (JSON.parse(response._body)) : {};
        this.helper.hideLoading();
        if(res && res.status == 'Success'){
          this.helper.showMessage('Hospital has been approved successfully', 3000, 'toast-success')
          this.init();
        }else{
          this.helper.showMessage('Failed to approve the hospital', 3000)
        }

      })
      .catch((err) => {
        this.helper.hideLoading();
        this.helper.showMessage('Failed to approve the hospital', 3000)
        console.log('err occured', err)
      })
  }

  async rejectHospital(hospitalData:any) {
    this.helper.showLoading();
     let hospital = hospitalData.hospitalDetail;
     hospital.hospitalId = hospitalData.hospitalId;
    this.helper.sendEmail(hospitalData.createdByEmail, {operationType: 'rejectHospital', userId:hospitalData.userId, hospital:hospital})
      .then((response:any) => {
        let res = typeof(response._body) == 'string' ? (JSON.parse(response._body)) : {};
        this.helper.hideLoading();
        if(res && res.status == 'Success'){
          this.helper.showMessage('Hospital has been rejected successfully', 3000, 'toast-success')
          this.init();
        }else{
          this.helper.showMessage('Failed to reject the hospital', 3000)
        }
      })
      .catch((err) => {
        this.helper.hideLoading();
        this.helper.showMessage('Failed to reject the hospital', 3000)
        console.log('err occured', err)
      })
  }

  updateHospitals() {
    console.log('in update hospitals')
  }
}
