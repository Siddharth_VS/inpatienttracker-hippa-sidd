import { Component, ViewChild } from '@angular/core';
import { NavParams, NavController } from 'ionic-angular';
import { Helper } from '../../providers/helper';
import { HomePage } from '../home/home';
import { PatientService } from '../../providers/patient-service';
import { StorageService } from '../../providers/storage-service';
import * as moment from 'moment';
import * as _ from 'underscore';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';

@Component({
  selector: 'page-add-patient',
  templateUrl: 'add-patient.html'
})

export class AddPatientPage {
  username: string;
  patient: any = {
    sex: 'M',
    dateOfBirth: '',
    dateOfAdmission: moment().format()
  };
  showBackButton: boolean = false;
  uid: any;
  mode: String;
  maxDate: any = moment().format('');
  currentHospitalId: string;
  moment = moment;
  currentDoctor: any = {};

  constructor(
    public barcodeScanner: BarcodeScanner,
    public helper: Helper,
    public navCtrl: NavController,
    public params: NavParams,
    public patientService: PatientService,
    public storageService: StorageService,
  ) {
    this.init()
  }

  async init() {
    this.currentDoctor = await this.storageService.getSettings('CURRENT_DOCTOR')
    this.showBackButton = this.params.get("showBackButton");
    this.currentHospitalId = this.params.get("hospitalId");
    this.mode = this.params.get("mode");
    if (this.params.get("patient")) {
      this.patient = _.clone(this.params.get("patient"));
      this.patient.dateOfAdmission = this.patient.dateOfAdmission ? moment(this.patient.dateOfAdmission, 'MM/DD/YYYY').format('') : moment().format();
      this.patient.dateOfBirth = moment(this.patient.dateOfBirth, 'MM/DD/YYYY').format('');
    }
  }

  getMinAdmissionDate() {
    return this.patient.dateOfBirth || moment().subtract(50, 'years').format('');
  }

  async savePatient() {
    try {
      this.helper.showLoading();
      this.patient['hospitalId'] = this.patient.hospitalId ? this.patient.hospitalId : this.currentHospitalId;

      Object.keys(this.patient).forEach((eachKey) => {
        if (eachKey == 'room') {
          this.patient[eachKey] = this.patient[eachKey] || null;
        }
        else {
          !this.patient[eachKey] ? delete this.patient[eachKey] : '';
        }
      })
      let patient = _.clone(this.patient);
      patient.dateOfAdmission = moment(patient.dateOfAdmission).format('MM/DD/YYYY');
      patient.dateOfBirth = moment(patient.dateOfBirth).format('MM/DD/YYYY');
      let res: any;
      if (this.helper.hasPermission('SWITCH_DOCTOR')) {
        patient.connectedUserAlias = this.currentDoctor.connectedUserAlias
        res = await this.patientService.upsertPatientDr(patient);
      } else {
        res = await this.patientService.upsertPatient(patient);
      }
      let patientId = _.values(res)[0].patientId, action = this.mode == 'EDIT' ? "UPDATE" : "CREATE";
      let patientEntitiesObj:any = {
        patientId: patientId,
        hospitalId: this.currentHospitalId,
        createdOn: moment().format('YYYYMMDD'),
        updatedOn: moment().format('YYYYMMDD'),
        billDetails: {
          diagnosis: [],
          em_code: [],
          procedures: []
        }
      }
      if (this.mode != 'EDIT') {
        patientEntitiesObj.connectedUserAlias = this.currentDoctor.connectedUserAlias
        this.helper.hasPermission('SWITCH_DOCTOR')
          ?
          await this.patientService.upsertPatientEntitiesForDr(patientEntitiesObj, action)
          :
          await this.patientService.upsertPatientEntities(patientEntitiesObj, action)
      }

      this.helper.hideLoading();
      this.helper.showMessage("Patient saved successfully");
      this.navCtrl.setRoot(HomePage);
    } catch (err) {
      console.log(err);
      this.helper.hideLoading();
      this.helper.showMessage("Error occured while saving patient");
    }
  }

  launchBarcodeScanner() {
    let self: any = this;
    this.barcodeScanner.scan()
      .then((barcodeData: any) => {
        self.patient.medicalRecordNumber = barcodeData.text || '';
      })
      .catch((err) => {
        console.log('Failed to lauch barcode scanner');
      })
  }
}
