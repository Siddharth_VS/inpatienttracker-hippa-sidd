import { Component, OnDestroy, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Events, MenuController, NavController, Slides } from 'ionic-angular';
import { HomePage } from '../home/home';
import { HospitalCard } from '../../components/hospital-card/hospital-card';
import { Helper } from '../../providers/helper';
import * as _ from 'underscore';
import { SubscriptionListing } from '../../subscriptions-module';
import { Auth } from 'aws-amplify';
import { LoginComponent } from '../authentication/login/login';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html'
})

export class WelcomePage implements OnDestroy {
  previousSlide: string;
  buttonText: string = 'Next';
  uid: any;
  valid: any = {
    security: false,
    hospitals: 0,
    specialization: false,
    subscription: true
  }
  userAppReadySubscription: any;
  nickname: any;
  @ViewChild(Slides) slides: Slides;

  constructor(
    public events: Events,
    public navCtrl: NavController,
    public helper: Helper,
    public menuCtrl: MenuController,
    public hospitalCard: HospitalCard,
    public storage: Storage
  ) {
    setTimeout(() => {
      this.slides.lockSwipes(true);
    });
    this.menuCtrl.swipeEnable(false);
    this.menuCtrl.enable(false);
    this.helper.getUid()
      .then((value: any) => {
        this.uid = value;
      });
    if (!helper.hasPermission('APP_LOCK') && helper.hasPermission('PDF_SECURITY')) {
      this.valid.security = true;
    }
  }

  slideToPrev() {
    this.slides.lockSwipes(false);
    this.slides.slidePrev();
    this.slides.lockSwipes(true);
  }

  slideToNext() {
    let self: any = this;
    // slide to next slide, if not the last
    this.slides.lockSwipes(false);
    if (this.slides.getActiveIndex() >= 1) {
      this.launchSubscriptionListing();
    }
    else {
      self.slides.slideNext();
      this.slides.lockSwipes(true);
    }
  }

  updateOptions(data: any) {
    let self = this;
    console.log(data);
    _.each(_.keys(data), function(value: any) {
      self.valid[value] = data[value]
    })
  }

  isValid() {
    if (this.helper.hasPermission('APP_LOCK,PDF_SECURITY')) {
      if (this.slides.getActiveIndex() == 0) {
        return this.valid.security;
      } else if (this.slides.getActiveIndex() == 1) {
        if (!this.helper.hasPermission('CHOOSE_SPECIALIZATION')) {
          return this.valid.hospitals;
        } else {
          return this.valid.hospitals && this.valid.specialization;
        }
      } else {
        return this.valid.subscription;
      }
    } else {
      if (this.slides.getActiveIndex() == 0) {
        if (!this.helper.hasPermission('CHOOSE_SPECIALIZATION')) {
          return this.valid.hospitals;
        } else {
          return this.valid.hospitals && this.valid.specialization;
        }
      }
      else {
        return this.valid.subscription;
      }
    }
  }

  finish(){
    this.events.publish('user:app-ready');
  }

  launchSubscriptionListing() {
    this.navCtrl.push(SubscriptionListing, { isWelcomeScreen: true });
  }

  ngOnDestroy() {
    this.menuCtrl.swipeEnable(true);
    this.menuCtrl.enable(true);
  }

}
