import { Component } from '@angular/core';
import { Events, NavParams, NavController } from 'ionic-angular';
import { PatientDetailPage } from '../../patient-details/patient-details';
import { Helper } from '../../../providers/helper';
import { PatientService } from '../../../providers/patient-service';
import * as moment from 'moment';
import * as _ from 'underscore';

@Component({
  selector: 'cart-detail',
  templateUrl: 'cart-detail.html'
})

export class CartDetail {
  patients: any = [];
  hospital: any;
  moment: any = moment;
  isEdit: boolean = false;
  patientDetailPage: any = PatientDetailPage
  selectFollowUpInWeeks: any[] = [];
  uid: any;
  selectOptions = {
    title: "Follow Up in"
  };

  queryRef: any;

  constructor(
    public navParams: NavParams,
    public navCtrl: NavController,
    public helper: Helper,
    public events: Events,
    public patientService: PatientService,
    // public storageService: StorageService
  ) {
    this.selectFollowUpInWeeks = Array.from(Array(10), (x, i) => i + 1);
    this.selectFollowUpInWeeks.unshift('None');
    this.hospital = this.navParams.data;
    this.loadCart();

  }

  loadCart() {
    let self = this;
    this.helper.showLoading();
    this.patientService.getInCartPatients(this.queryRef, this.hospital.hospitalId)
      .then((result: any) => {
        result.take(1)
          .subscribe((res: any) => {
            res = JSON.parse(JSON.stringify(res.queryInCartPatientDetails.items));
            this.patients = [];
            _.each(res, (patient) => {
              patient.followUpInWeeks = patient.followUpInWeeks || 'None';
              self.patients.push(patient);
            });
          })
      })
    this.helper.hideLoading();
  }

  getFollowUpTitle(followUp: any) {
    return followUp == 'None' ? `${followUp}` : `${followUp} week(s)`;
  }

  getFollowUpValue(followUpValue: any) {
    followUpValue = followUpValue || 'None';
    return followUpValue;
  }

  async changeFollowUp(patient: any) {
    try{
      let temp = {
        hospitalId: patient.hospitalId,
        patientId: patient.patientId,
        followUpInWeeks: patient.followUpInWeeks
      }
      await this.patientService.upsertPatient(temp);
    }catch(err){
      console.log(err);
    }
  }

  toggleSelect() {
    this.isEdit = !this.isEdit;
  }

  // remove patient from cart w.r.t hospital
  removePatient(patient: any) {
    this.helper.showLoading();
    let index = _.findIndex(this.patients, patient);

    if (index > -1) {
      this.patients.splice(index, 1);

      let temp = {
        hospitalId: patient.hospitalId,
        patientId: patient.patientId,
        status: "ACTIVE"
      }
      this.patientService.upsertPatient(temp)
        .then(() => {
          return this.helper.getInCartPatients(patient.hospitalId)
        })
        .then(() => {
          this.helper.hideLoading();
        })
        .catch(() => {
          this.helper.hideLoading();
        })
    }
  }

  ngOnDestroy() {
    this.queryRef ? this.queryRef.unsubscribe() : '';
  }

  selectPatient(patient: any) {
    if (patient.isSelected) {
      delete patient.isSelected;
    } else {
      patient.isSelected = true;
    }
  }

  bulkBilling(actionType: any) {
    let selectedPatients: any = [], patientIds: any = [];

    selectedPatients = _.filter(this.patients, function(patient: any) {
      if (patient.isSelected) {
        return patient
      }
    });

    if (this.patients.length && !selectedPatients.length) {
      selectedPatients = this.patients;
    }

    _.each(selectedPatients, function(patient: any) {
      patientIds.push(patient.patientId);
    })

    this.helper.bulkBilling(selectedPatients, patientIds, this.hospital, actionType);

    this.events.subscribe('billing:done', () => {
      if (this.isEdit)
        this.toggleSelect()
      this.loadCart();
    });
  }

}
