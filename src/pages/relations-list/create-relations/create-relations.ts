import { Component } from '@angular/core';
import { ModalController, NavParams, ViewController } from 'ionic-angular';
import { HospitalService } from '../../../providers/hospitalService';
import { Helper } from '../../../providers/helper';
import * as _ from 'underscore';
import { RolesManagementPage } from '../../roles-management/roles-management';

@Component({
  selector: 'page-create-relations',
  templateUrl: 'create-relations.html'
})

export class CreateRelationsPage {
  selectedRole: string = '';
  selectedHospitalId: string = '';
  relations: any = [];
  selectedRelation: any = {};
  preSelectedNurses: any = [];

  constructor(
    public helper: Helper,
    public hospitalService: HospitalService,
    public modalCtrl: ModalController,
    public navParams: NavParams,
    public viewCtrl: ViewController
  ) {
    this.init()
  }

  init() {
    this.selectedRole = this.navParams.get('role') || ''
    this.selectedHospitalId = this.navParams.get('hospitalId') || ''
    this.preSelectedNurses = this.navParams.get('selectedNurses') || []
    if (this.selectedRole == 'Nurse') {
      this.listNurses()
    } else {
      this.listSecretaries()
    }
  }

  listNurses() {
    let self: any = this;
    this.hospitalService.getNurseListForHospital(this.selectedHospitalId)
      .then((response) => {
        this.relations = response;
        console.log('self.preSelectedNurses', self.preSelectedNurses)
        console.log('self.relations', self.relations)
        _.each(self.preSelectedNurses.selectedNurses, function(preSelectedNurse: any) {
          _.each(self.relations, function(nurse: any) {
            if ((preSelectedNurse.connectedUserAlias == nurse.userAliasId)) {
              nurse.isSelected = true;
              nurse.icon = '';
            }
          })
        });
      })
      .catch((err) => {
        console.log('err occured', err)
      })
  }

  listSecretaries() {
    this.hospitalService.getSecretaryListForHospital(this.selectedHospitalId)
      .then((response) => {
        this.relations = response;
      })
      .catch((err) => {
        console.log('err occured')
      })
  }

  checkRelation(relation: any) {
    if (!relation.isSelected) {
      _.each(this.relations, function(value: any) {
        value.icon = '';
      });
      relation.icon = 'md-checkmark'
      this.selectedRelation = relation
    }
  }

  createRelation() {
    let modal = this.modalCtrl.create(RolesManagementPage, {
      title: 'Add Permissions',
      isModal: true,
      userType: this.selectedRole,
      relation: {
        connectedUserAliasId: this.selectedRelation.userAliasId,
        hospitalId: this.selectedHospitalId
      }
    })
    modal.present();
  }

  isFormValid() {
    return !(this.selectedRelation && this.selectedRelation.userAliasId && this.selectedRelation.icon)
  }
}
