import { Component } from '@angular/core';
import { AlertController, Events, ModalController, NavController, NavParams, Platform } from 'ionic-angular';
import { Helper } from '../../../../providers/helper';
import { StorageService } from '../../../../providers/storage-service';
import { HospitalService } from '../../../../providers/hospitalService';
import { ManageCard } from '../manage-card/manage-card';

declare const ImageCapturePlugin: any;
import { Camera, CameraOptions } from "@ionic-native/camera";

@Component({
  selector: 'cards-listing',
  templateUrl: 'cards-listing.html'
})
export class HospitalCardsListing{
  card:any = {};
  currentHospital: any = { hospitalId: null };
  //camera configuration
  options: CameraOptions = {
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE,
    correctOrientation: true,
    quality: 100,
    allowEdit: true,
    destinationType: 0,
  };
  currentSegment:string = 'public';
  customCards:any = [];
  hospitalCards:any = [];
  cards:any = [];
  screenType:string = 'initial';
  currentSelectedCard:any = {};

  constructor(
    public alertCtrl: AlertController,
    public camera: Camera,
    public events: Events,
    public helper: Helper,
    public hospitalService: HospitalService,
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public platform: Platform,
    public storageService: StorageService
  ){}

ionViewDidEnter(){
  let self:any = this;
  this.screenType = this.navParams.get('screenType') || 'initial';
  this.events.subscribe(('reload-card-listing'), ()=>{
    console.log('inside subscription', this)
    this.init();
  })
  this.init()
}

async init(){
  try{
    this.helper.showLoading();
    this.currentSegment = 'public'
    this.currentHospital = (await this.storageService.getSettings("HOSPITAL_DATA")) || {};
    if(this.screenType == 'pendingApprovalList'){
      this.cards = await this.hospitalService.getApprovalPendingCards(this.currentHospital.hospitalId)
      this.helper.hideLoading();
    }else{
      this.customCards = await this.hospitalService.getCustomCards(this.currentHospital.hospitalId);
      this.hospitalCards = await this.hospitalService.getHospitalCards(this.currentHospital.hospitalId);
      this.cards = this.hospitalCards;
      this.helper.hideLoading();
    }
  }catch(err){
    this.helper.hideLoading();
    console.log('err occured',err)
  }
}

onCardSelection(res:any){
  if((res.card || {}).cardId){
    this.currentSelectedCard = res.card;
  }
}

createNewCard(){
  let alert = this.alertCtrl.create({
    title: "Add new Card format",
    message: "Are you sure want to add a new card format to the hospital",
    cssClass: "add-patient-alert",
    buttons: [{
      text: "Cancel",
      role: 'cancel'
    },
    {
      text: "Continue",
        handler: () => {
          this.scanCard();
          // this.scanCardTemp();
        }
    }
  ]
  });
  alert.present();
}

scanCardTemp(){
  let modal = this.modalCtrl.create(ManageCard, { rawImage: JSON.parse(localStorage.getItem('scanned-card')), image: localStorage.getItem('capturedBase64data'), currentHospital: this.currentHospital, cardMappingType: 'mapping_creation', showBackButton: true });
      modal.present();
      // this.navCtrl.push(ManageCard, { rawImage: JSON.parse(localStorage.getItem('scanned-card')), image: localStorage.getItem('capturedBase64data'), currentHospital: this.currentHospital, cardMappingType: 'patient_creation_scan', showBackButton: true })
      // this.navCtrl.push(ManageCard, { rawImage: JSON.parse(localStorage.getItem('scanned-card')), image: localStorage.getItem('capturedBase64data'), currentHospital: this.currentHospital, cardMappingType: 'patient_creation_scan', showBackButton: true })
}

scanCard(){
  let self: any = this,
  image = '';
  new Promise((resolve, reject) => {
    self.helper.showLoading();
    if (this.platform.is('ios')) {
      ImageCapturePlugin.takePhoto([350, 160], function(data) {
        this.helper.showLoading();
        localStorage.setItem('capturedBase64data', data);
        resolve(data);
      }, (err) => {
        this.helper.hideLoading();
        console.log(err);
        reject(err);
      })
    }
    else {
      this.camera.getPicture(this.options)
        .then((base64Image: any) => {
          // imageData is either a base64 encoded string or a file URI
          // If it's base64:
          localStorage.setItem('capturedBase64data', base64Image);
          resolve(base64Image);
        })
        .catch((err) => {
          console.log(err);
          this.helper.hideLoading();
          reject(err);
        });
    }
  })
    .then((base64Image: any) => {
      image = base64Image;
      return this.helper
        .scanCard(base64Image, this.currentHospital.code, 'raw');
    })
    .then((res: any) => {
      console.log('res',res)
      console.log('this.card.error')
      this.card = res.card;
      // if (this.card.error) {
      //   this.helper.showMessage('Failed to scan, please try again');
      // }
      // else {

      console.log('inside the response res:', res)
      // }
      if(this.screenType == 'cardSelectionForScanning'){
        this.navCtrl.push(ManageCard, { rawImage: res.labels, image: localStorage.getItem('capturedBase64data'), currentHospital: this.currentHospital,hospitalId: this.currentHospital.hospitalId,  cardMappingType: 'patient_creation_scan', showBackButton: true, cardData: this.currentSelectedCard })
      }else{
        let modal = this.modalCtrl.create(ManageCard, { rawImage: res.labels, hospitalId: this.currentHospital.hospitalId, image: image, currentHospital: this.currentHospital, cardMappingType: 'mapping_creation' });
        modal.present();
      }
      this.helper.hideLoading();
    })
    .catch((err) => {
      this.helper.hideLoading();
    });
  }

  onSegmentChange(){
    if(this.currentSegment == 'public'){
      this.cards = this.hospitalCards;
    }else{
      this.cards = this.customCards;
    }
  }

  reloadListing(){
    this.init()
  }
  continueCardScanning(){
    console.log('continueCardScanning')
    if(this.currentSelectedCard && this.currentSelectedCard.cardId){
      this.scanCard()
    }
  }

  scanCardTempForPtntCrtn(){
    this.navCtrl.push(ManageCard, { rawImage: JSON.parse(localStorage.getItem('scanned-card')), image: localStorage.getItem('capturedBase64data'), currentHospital: this.currentHospital, cardMappingType: 'patient_creation_scan', showBackButton: true })
  }

  isContinueBtnDisabled(){
    return !(this.currentSelectedCard && this.currentSelectedCard.cardId);
  }
}
