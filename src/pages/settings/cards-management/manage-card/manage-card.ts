import { Component, ElementRef, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { AlertController, Events, NavController, NavParams, ViewController } from 'ionic-angular';
import { NgForm } from '@angular/forms';

import { Helper } from '../../../../providers/helper';

import { StorageService } from '../../../../providers/storage-service';
import { HomePage } from '../../../home/home';
import { PatientService } from '../../../../providers/patient-service';
import { HospitalService } from '../../../../providers/hospitalService';

import * as _ from 'underscore';
declare var html2canvas:any;
import * as moment from 'moment';
import { Auth, Storage } from 'aws-amplify'
@Component({
  selector: 'manage-card',
  templateUrl: 'manage-card.html'
})
export class ManageCard{

constructor(
  public alertCtrl: AlertController,
  public events: Events,
  public helper: Helper,
  public hospitalService: HospitalService,
  public navCtrl: NavController,
  public navParams: NavParams,
  public patientService: PatientService,
  public storageService: StorageService,
  public viewCtrl: ViewController
){}

  @ViewChild('scannedCardImg') scannedCardImg: ElementRef;
  @ViewChild('patientFormElement') setForm(f: any) {
  this.form = f;
  }
   form: any;

  card:any = {};
  image:any = '';
  rawImage:any = {};
  currentHospital:any = {};
  lines:any[] = [];
  _ = _;
  imageData:any = {height: 1, width: 1};
  isMapping:boolean = false;
  mappingCompleted:boolean = false;
  attributes :any[] = [{name: 'First Name', regex:'[A-Za-z0-9_]+', ngModelKey: 'name', required: true, isMaskingRequired: true },
                       {name: 'Middle Name', regex:'[A-Za-z0-9_]+', ngModelKey: 'name', required: true, isMaskingRequired: true },
                       {name: 'Last Name', regex:'[A-Za-z0-9_]+', ngModelKey: 'name', required: true, isMaskingRequired: true },
                       {name: 'MR No', regex: '[0-9]+', ngModelKey: 'medicalRecordNumber', isMaskingRequired: true },
                       {name: 'FIN', regex: '[0-9]+', ngModelKey: 'medicalRecordNumber', isMaskingRequired: true },
                       {name: 'Account Number', regex: '[0-9]+', ngModelKey: 'accountNumber', isMaskingRequired: true },
                       {name: 'Gender', regex: '^[\F\M]{1}$', ngModelKey: 'sex' },
                       {name: 'DOB', isDate: true, regex: '^([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)\d{4}$', ngModelKey: 'dateOfBirth' },
                       {name: 'Admission Date', isDate: true, regex: '^([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)\d{4}$', ngModelKey: 'dateOfAdmission' },
                       {name: 'Referring Doctor', regex: '[A-Za-z0-9_]+', required: true, ngModelKey: 'referringDoctor' },
                       {name: 'Age', regex: '[0-9]+', ngModelKey: 'age' }
                     ];
  /**Note: Attributes is used while extracting the text and creating mapping flow **/
  isEdit:boolean = false;
  currentSelection: any = { lineIndex: -1, wordIndex: -1, word: ''};
  cardMappingType:any = 'mapping_creation'; //cardMappingType holds "mapping_creation" or "patient_creation"
  containerType: string = '';
  cardId:string = '';
  cardName:string = '';

  /*variables used during patient card scanning*/
  currentSelectedBoxes:any = [];
  currentSelectedAttribute:any =  {};
  username: string;
  patient: any = {
    sex: 'M',
    dateOfBirth: '',
    dateOfAdmission: moment().format()
  };
  showBackButton: boolean = false;
  uid: any;
  mode: String;
  maxDate: any = moment().format('');
  currentHospitalId: string;
  moment = moment;
  currentDoctor: any = {};
  cardData:any = {};

  ngOnInit(){
    this.helper.showLoading();
    this.currentHospital = this.navParams.get('currentHospital');
    this.cardMappingType = this.navParams.get('cardMappingType');
    if(this.cardMappingType != 'patient_creation_manual'){
      this.rawImage = this.navParams.get('rawImage');
      this.image = this.navParams.get('image');
      let groupedEntries = _.groupBy(this.rawImage, (entry) => {
        return entry.Type;
      });
      this.lines = _.groupBy(groupedEntries.WORD, (word) => {
        return word.ParentId;
      });
    }
    if(this.cardMappingType == 'patient_creation_scan'){
      this.cardData = this.navParams.get('cardData');
      this.extractData()
    }
    if(this.cardMappingType == 'patient_creation_manual' || this.cardMappingType == 'patient_creation_scan'){
      this.init()
    }else if(this.cardMappingType == 'cardApproval'){
      this.currentHospital = this.navParams.get('currentHospital');
      this.card = this.navParams.get('card');
      console.log('this.cardDetails', this.card)
      this.helper.hideLoading();
    }else{
      // this.isEdit = true
      this.helper.hideLoading();
      this.containerType = 'boundingBox';
    }
  }

  ionViewDidLoad(){
    setTimeout(()=>{
      if(this.scannedCardImg){
        this.imageData.height = this.scannedCardImg.nativeElement.offsetHeight;
        this.imageData.width = this.scannedCardImg.nativeElement.offsetWidth;
      }
      // this.form = document.getElementById('patientFormElement')
    }, 500)
  }

  toggleMapping(){
    this.isMapping = !!!this.isMapping;
    if(this.isMapping){
      this.currentSelection.wordIndex = this.currentSelection.lineIndex = 0;
      this.currentSelection.word = this.lines[0][0].DetectedText;
    }
  }

  done(){
    let self:any = this;
    this.currentSelection.wordIndex = this.currentSelection.lineIndex = -1
    this.currentSelection.word = ''
    this.toggleMapping()
    this.mappingCompleted = true;
  }


  nextSelection(){
    this.currentSelection.word  = ''
    if(this.lines[this.currentSelection.lineIndex] && this.lines[this.currentSelection.lineIndex][this.currentSelection.wordIndex + 1]){
      this.currentSelection.wordIndex = this.currentSelection.wordIndex + 1;
    }else{
      if(this.lines[this.currentSelection.lineIndex + 1]){
        this.currentSelection.lineIndex += 1;
        this.currentSelection.wordIndex = 0;
      }else{
        this.currentSelection.lineIndex = 0;
        this.currentSelection.wordIndex = 0;
      }
    }
    this.currentSelection.word = this.lines[this.currentSelection.lineIndex][this.currentSelection.wordIndex].DetectedText
  }

  PrevSelection(){
    if(this.lines[this.currentSelection.lineIndex] && this.lines[this.currentSelection.lineIndex][this.currentSelection.wordIndex - 1]){
      this.currentSelection.wordIndex = this.currentSelection.wordIndex - 1;
    }else{
      if(this.lines[this.currentSelection.lineIndex - 1]){
        this.currentSelection.wordIndex = this.lines[this.currentSelection.lineIndex - 1].length - 1;
        this.currentSelection.lineIndex -= 1;
      }else{
        this.currentSelection.lineIndex = 0;
        this.currentSelection.wordIndex = 0;
      }
    }
    this.currentSelection.word = this.lines[this.currentSelection.lineIndex][this.currentSelection.wordIndex].DetectedText
  }

  onSelection(data:any){
    if(data.containerType == 'textExtraction'){
      let index = _.findIndex(this.currentSelectedBoxes, (ele)=>{
          return ele.lineIndex == data.lineIndex && ele.wordIndex == data.wordIndex;
        })
        if(index < 0){
          let word = this.processDetectedText(this.attributes[data.attributeIndex], data.DetectedText)
          if(this.attributes[data.attributeIndex].isDate){
          this.currentSelectedBoxes = [];
          }
          this.currentSelectedBoxes.push({ wordIndex:  data.wordIndex, lineIndex: data.lineIndex, DetectedText: word });

          if(this.currentSelectedBoxes.length){
            if(this.attributes[data.attributeIndex].isDate){

              this.patient[this.currentSelectedAttribute.ngModelKey] = moment(word, 'MM/DD/YYYY').format('')
            }else{
              this.patient[this.currentSelectedAttribute.ngModelKey] =  ( this.patient[this.currentSelectedAttribute.ngModelKey] ? this.patient[this.currentSelectedAttribute.ngModelKey] : "" )  + " " + ( word ? word : "" )
            }
          }else{
            this.patient[this.currentSelectedAttribute.ngModelKey] = ""
          }

        }else{
          this.helper.showMessage("Already data extracted");
        }
    }else{
      if(!this.isMapping){
        this.isMapping = true
      }
      this.currentSelection.lineIndex = data.lineIndex;
      this.currentSelection.wordIndex = data.wordIndex;
      this.currentSelection.word = this.lines[this.currentSelection.lineIndex][this.currentSelection.wordIndex].DetectedText
    }
  }

  map(attributeName:string, index:number){
    let self:any = this;
    let i = _.findIndex(this.attributes, function(attribute){
      return (attribute.mappedData || {}).boundingArea && attribute.mappedData.boundingArea.lineIndex == self.currentSelection.lineIndex && attribute.mappedData.boundingArea.wordIndex == self.currentSelection.wordIndex
    })
    if(i < 0){
        if((this.attributes[index].mappedData || {}).boundingArea){
          if(this.attributes[index].mappedData.boundingArea.lineIndex == this.currentSelection.lineIndex && this.attributes[index].mappedData.boundingArea.wordIndex == this.currentSelection.wordIndex){
            this.attributes[index].mappedData = {};
            this.helper.showMessage('removed the attribute mapping');
          }else{
            this.showMappedAlert(attributeName)
          }
        }else{
          this.attributes[index].mappedData = { boundingArea: {lineIndex: this.currentSelection.lineIndex, wordIndex: this.currentSelection.wordIndex }, boundingWord: this.currentSelection.word};
          this.attributes[index].mappedData.boundingWord = this.processDetectedText(this.attributes[index])
          this.helper.showMessage('Attribute Mapping done');
        }
    }
    else if(i >= 0 && (this.attributes[index].mappedData || {}).boundingArea && this.attributes[index].mappedData.boundingArea.lineIndex == this.currentSelection.lineIndex && this.attributes[index].mappedData.boundingArea.wordIndex == this.currentSelection.wordIndex){
      this.attributes[index].mappedData = {};
      this.helper.showMessage('removed the attribute mapping');
    }
      else{
      this.showMappedAlert(attributeName)
    }
  }

  processDetectedText(attribute:any, text?:string){
    let word = (attribute.mappedData || {}).boundingWord || "";
    if(text){
      word = text;
    }
    let regex = new RegExp(attribute.regex, 'g')
    if(!attribute.isDate){
      if(regex.test(word)){
        let res = word.match(regex);
        if(typeof(res) != 'string'){
          word = res.join(' ');
        }
      }
    }else{
      let res = word.match(/[0-9_/]*$/g)
      res = _.compact(res);
      console.log('res', res)
      word = res[0];
      // _.each(res, (item)=>{
      //    if(regex.test(item)){
      //      word = item
      //     return false;
      //    }
      // })
    }
    return word;
  }

  showMappedAlert(attributeName:string){
    let alert = this.alertCtrl.create({
              title: 'Already Mapped!',
              subTitle: "Already " + attributeName + " has been mapped to another label, release/remove it to assign new one",
              buttons: ['OK']
            });
    alert.present();
  }

  getBtnClass(attribute:any){
  if((attribute.mappedData || {}).boundingArea && attribute.mappedData.boundingArea.lineIndex == this.currentSelection.lineIndex && attribute.mappedData.boundingArea.wordIndex == this.currentSelection.wordIndex){
      return 'selected-attribute-btn';
  }
  else if((attribute.mappedData || {}).boundingArea){
    return 'assigned-attribute-btn';
  }
  return '';
  }

  async init() {
      this.currentDoctor = await this.storageService.getSettings('CURRENT_DOCTOR')
      this.showBackButton = this.navParams.get("showBackButton");
      this.currentHospitalId = this.navParams.get("hospitalId");
      this.mode = this.navParams.get("mode");
      if (this.navParams.get("patient")) {
        this.patient = _.clone(this.navParams.get("patient"));
        this.patient.dateOfAdmission = this.patient.dateOfAdmission ? moment(this.patient.dateOfAdmission, 'MM/DD/YYYY').format('') : moment().format();
        this.patient.dateOfBirth = moment(this.patient.dateOfBirth, 'MM/DD/YYYY').format('');
      }
      this.helper.hideLoading();
    }
    getMinAdmissionDate() {
    return this.patient.dateOfBirth || moment().subtract(50, 'years').format('');
  }

  async savePatient() {
    try {
      this.helper.showLoading();
      this.patient['hospitalId'] = this.patient.hospitalId ? this.patient.hospitalId : this.currentHospitalId;

      Object.keys(this.patient).forEach((eachKey) => {
        if (eachKey == 'room') {
          this.patient[eachKey] = this.patient[eachKey] || null;
        }
        else {
          !this.patient[eachKey] ? delete this.patient[eachKey] : '';
        }
      })
      let patient = _.clone(this.patient);
      patient.dateOfAdmission = moment(patient.dateOfAdmission).format('MM/DD/YYYY');
      patient.dateOfBirth = moment(patient.dateOfBirth).format('MM/DD/YYYY');
      let res: any;
      if (this.helper.hasPermission('SWITCH_DOCTOR')) {
        patient.connectedUserAlias = (this.currentDoctor || {}).connectedUserAlias
        res = await this.patientService.upsertPatientDr(patient);
      } else {
        res = await this.patientService.upsertPatient(patient);
      }
      let patientId = _.values(res)[0].patientId, action = this.mode == 'EDIT' ? "UPDATE" : "CREATE";
      let patientEntitiesObj:any = {
        patientId: patientId,
        hospitalId: this.currentHospitalId,
        createdOn: moment().format('YYYYMMDD'),
        updatedOn: moment().format('YYYYMMDD'),
        billDetails: {
          diagnosis: [],
          em_code: [],
          procedures: []
        }
      }
      if (this.mode != 'EDIT') {
        patientEntitiesObj.connectedUserAlias = (this.currentDoctor || {}).connectedUserAlias
        this.helper.hasPermission('SWITCH_DOCTOR')
          ?
          await this.patientService.upsertPatientEntitiesForDr(patientEntitiesObj, action)
          :
          await this.patientService.upsertPatientEntities(patientEntitiesObj, action)
      }

      this.helper.hideLoading();
      this.helper.showMessage("Patient saved successfully");
      this.navCtrl.setRoot(HomePage);
    } catch (err) {
      console.log(err);
      this.helper.hideLoading();
      this.helper.showMessage("Error occured while saving patient");
    }
  }

  isFormInValid(form:NgForm){
    if(form){
      return form.invalid;
    }
    return true
  }

  onFocus(data){
    console.log('data', data)
    this.containerType = '';
    this.currentSelectedBoxes = [];
    this.currentSelectedAttribute = {};
  }

  startExtraction(key:string, ngModelKey:string, isMultipleSelection: boolean){
    this.containerType = 'textExtraction';
    this.currentSelectedBoxes = [];
    let index = _.findIndex(this.attributes, (ele)=>{
      return ele.name == key
    })
    this.currentSelectedAttribute = { key, ngModelKey, attributeIndex: index };
  }

  extractData(){
    let self:any = this;
    console.log('this.cardData', this.cardData)
    if((this.cardData.cardDetails || []).length){
      _.each(this.cardData.cardDetails,(ele)=>{
        let detectedText = self.lines[ele.lineIndex] && self.lines[ele.lineIndex][ele.wordIndex] && self.lines[ele.lineIndex][ele.wordIndex].DetectedText ? self.lines[ele.lineIndex][ele.wordIndex].DetectedText : '';
        let index = _.findIndex(self.attributes, {name: ele.name});
        if(index >-1 && (self.attributes[index] || {}).name){
          detectedText = self.processDetectedText(self.attributes[index], detectedText)
        }
        if(self.attributes[index].isDate){
          self.patient[self.attributes[index].ngModelKey] = moment(detectedText, 'MM/DD/YYYY').format('')
        }else{
          if(self.attributes[index].ngModelKey == 'name'){
            if((self.patient[self.attributes[index].ngModelKey] == '' || typeof(self.patient[self.attributes[index].ngModelKey]) == "undefined")){
              self.patient[self.attributes[index].ngModelKey] = detectedText;
            }else{
              self.patient[self.attributes[index].ngModelKey] += " " + (detectedText || "");
            }
          }else{
            self.patient[self.attributes[index].ngModelKey] = detectedText;
          }
        }
      })
    }
  }

  launchAlert(){
    let self:any =  this;
    const prompt = this.alertCtrl.create({
      title: 'Create Card',
      message: "Enter a name for this new card",
      inputs: [
        {
          name: 'name',
          placeholder: 'Name'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Continue',
          handler: data => {
            self.saveMapping(data.name)
          }
        }
      ]
    });
    prompt.present();
  }

  async saveMapping(cardName: String){
    try{
      this.helper.showLoading();
      this.containerType = 'masked';
      let element = document.getElementById("capture"),
      self:any = this;
      let res = await self.helper.generateUUID();
      self.cardId = res.uuid;
      html2canvas(element, {
        onrendered: async function(canvas) {
          let  dataURL = canvas.toDataURL(),
          img:any = document.getElementsByClassName("card-scan-img-1")[0];
          let res = await self.helper.generateUUID();
          Storage.put(`${self.cardId}.png`, new Buffer(dataURL.split('data:image/png;base64,')[1], 'base64'), {
            ContentEncoding: 'image/png'
          })
          .then(async(result:any) =>{
            console.log('this.attributes',self.attributes)
            let cardDetails:any = _.compact(_.map(self.attributes, (ele) => {
              let temp:any = (ele.mappedData ? _.pick(ele, ['name', 'mappedData']) : '')
              if(typeof(temp) != 'string'){
                temp.lineIndex = ((temp.mappedData||{}).boundingArea || {}).lineIndex;
                temp.wordIndex = ((temp.mappedData||{}).boundingArea || {}).wordIndex;
                temp.extractedText = (temp.mappedData||{}).boundingWord;
                ele.isDate ? temp.isDate = ele.isDate : '';
                delete temp.mappedData
              }
              console.log('temp',temp)
              return temp;
            }));
            console.log('cardDetails',cardDetails)
            let email = '';
            try{
              email = await self.storageService.getSettings('email');
            }catch(err){
              console.log('err occured',err)
              self.helper.hideLoading();
              self.viewCtrl.dismiss();
              self.helper.showMessage('Failed to save mapping, please try again');
            }
            return  self.hospitalService.createCard({
              hospitalId : self.currentHospital.hospitalId,
              hospitalName : self.currentHospital.hospitalDetail ? self.currentHospital.hospitalDetail.name : self.currentHospital.name ,
              cardId : self.cardId,
              isCustom : true,
              name: cardName,
              s3ImageKey: typeof(result) == 'string' ? result: result.key,
              approvalStatus: 'created',
              cardDetails: cardDetails,
              createdByEmail: email
            });
          })
          .then(()=>{
            self.helper.hideLoading();
            self.events.publish('reload-card-listing')
            self.viewCtrl.dismiss();
            self.helper.showMessage('Card has been created successfully');
          })
          .catch((err) => {
          console.log(err)
          self.helper.hideLoading();
          self.navCtrl.pop();
          self.helper.showMessage('Failed to save mapping, please try again');
          });
        }
      });
    }catch(err){
      this.helper.hideLoading();
      this.viewCtrl.dismiss();
      this.helper.showMessage('Failed to save mapping, please try again');
      console.log('err occured',err)
    }
  }

  async approveCard(){
    try{
      this.helper.showLoading();
      let res = await this.helper.generateUUID();
      this.helper.manageCard({cardData: { ...this.card, newCardId:res.uuid, approvedDate: moment.utc().format('x') }, operationType: 'approveCard', hospital: this.currentHospital })
      .then((res:any)=>{
        console.log('in manageCard', res)
        this.helper.hideLoading();
        this.navCtrl.pop();
        this.helper.showMessage('Card has been approved successfully');
      })
      .catch((err)=>{
        this.helper.hideLoading();
        this.navCtrl.pop();
        this.helper.showMessage('Failed to approve the card, please try again later');
        console.log('err occured',err)
      })
    }catch(err){
      this.helper.hideLoading();
      this.navCtrl.pop();
      this.helper.showMessage('Failed to approve the card, please try again later');
      console.log('err occured',err)
    }
  }

  launchRejectAlert(){
    let self:any = this;
    const prompt = this.alertCtrl.create({
      title: 'Reject Public card request',
      message: "Enter a reason for rejecting the card",
      inputs: [
        {
          name: 'reason',
          placeholder: 'Reason'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Continue',
          handler: data => {
            self.rejectCard(data.reason)
          }
        }
      ]
    });
    prompt.present();
  }

  rejectCard(rejectReason:any){
    this.helper.showLoading();
    this.helper.manageCard({cardData: this.card, operationType: 'rejectCard', hospital: this.currentHospital, rejectReason: rejectReason })
    .then((res:any)=>{
      this.helper.hideLoading();
      this.navCtrl.pop();
      this.helper.showMessage('Card has been rejected successfully');
      console.log('in manageCard', res)
    })
    .catch((err)=>{
      this.navCtrl.pop();
      this.helper.hideLoading();
      this.helper.showMessage('Failed to reject the card, please try again later');
      console.log('err occured',err)
    })
  }
}
