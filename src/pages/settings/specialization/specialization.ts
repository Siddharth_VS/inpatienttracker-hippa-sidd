import { Component } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';
import { Helper } from '../../../providers/helper';
import { SpecializationService } from '../../../providers/specialization-service';
import { StorageService } from '../../../providers/storage-service';
import * as _ from 'underscore';

@Component({
  selector: 'specialization',
  templateUrl: 'specialization.html'
})

export class SpecializationPage {
  selected: any;
  current: any;
  constructor(
    public helper: Helper,
    public nav: NavParams,
    public viewCtrl: ViewController,
    public service: SpecializationService,
    public storage: StorageService
  ) {
    this.storage.getSettings('SPECIALIZATION_ID')
      .then((value)=>{
        this.current = (value !== null)? value : this.current;
        this.selected = this.current || this.selected;
      })
  }

  save() {
    this.storage.setSettings('SPECIALIZATION_ID', this.selected)
      .then(()=>{
        this.viewCtrl.dismiss(this.selected);
      }, (err:any)=>{
        console.log(err);
      });
  }

  isValid() {
    return this.current && (this.current == this.selected);
  }

}
