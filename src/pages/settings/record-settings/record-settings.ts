import { Component } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';
import { Helper } from '../../../providers/helper';
import { StorageService } from '../../../providers/storage-service'
import * as _ from 'underscore';

@Component({
  selector: 'record-settings-page',
  templateUrl: 'record-settings.html'
})

export class RecordSettingsPage {
  recordSettings: any = [
    { name: 'procedures', key: 'cptCodes', label: 'Procedures', index: 0, isEnabled: true },
    { name: 'em_code', key: 'visitCodes', label: 'EM Codes', index: 1, isEnabled: true },
    { name: 'diagnosis', key: 'icdCodes', label: 'Diagnoses', index: 2, isEnabled: true },
    { name: 'notes', key: 'notes', index: 3, label: 'Notes', isEnabled: true },
  ];
  reorder: boolean = false;
  oldSettings: any = [];
  uid: string;
  showSwitch: boolean = true;
  constructor(
    public helper: Helper,
    public nav: NavParams,
    public viewCtrl: ViewController,
    public storageService: StorageService
  ) {
    this.helper.getUid()
      .then((uid) => {
        this.uid = uid;
        this.storageService.getSettings('RECORDS')
          .then((recordSettings) => {
            this.recordSettings = _.uniq(((recordSettings || []).length ? recordSettings : this.recordSettings), function(recordType: any) { return recordType.name });
            this.oldSettings = JSON.parse(JSON.stringify(this.recordSettings))
          })
      })
  }

  checkIsSaveable() {
    return _.isEqual(this.oldSettings, this.recordSettings);
  }

  enableReordering() {
    this.showSwitch = false;
    this.reorder = true;
  }

  disableReordering() {
    this.helper.showLoading();
    this.reorder = false;
    setTimeout(() => {
      this.showSwitch = true;
      this.helper.hideLoading();
    }, 500)
  }

  resetReorder() {
    this.reorder = false;
    setTimeout(() => {
      this.showSwitch = true;
    }, 500)
  }

  saveRecordSettings() {
    this.recordSettings = _.each(this.recordSettings, (recordType: any, index: any) => {
      recordType.index = index;
    })
    this.storageService.setLocalSettings('RECORDS', this.recordSettings)
      .then(() => {
        this.resetReorder();
        this.oldSettings = JSON.parse(JSON.stringify(this.recordSettings));
        this.helper.showMessage("Record settings have been updated.");
      })
      .catch((err: any) => {
        this.resetReorder();
        console.log(err);
      })
  }

  reorderItems(indexes: any) {
    let element = this.recordSettings[indexes.from];
    this.recordSettings.splice(indexes.from, 1);
    this.recordSettings.splice(indexes.to, 0, element);
  }
}
