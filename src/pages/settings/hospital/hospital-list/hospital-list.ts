import { Component } from '@angular/core';
import { ModalController, NavParams, ViewController } from 'ionic-angular';
import { HospitalService } from '../../../../providers/hospitalService';
import { Helper } from '../../../../providers/helper';
import { CreateHospital } from '../create-hospital/create-hospital';
import { AddHospital } from '../add-hospital/add-hospital';
import * as _ from 'underscore';
import { Storage } from '@ionic/storage';
@Component({
  selector: 'hospital-list',
  templateUrl: 'hospital-list.html'
})

export class HospitalList {
  hospitals: any[] = [];
  cloudhospitals = [];
  localHospitals: any = [];
  selectedHospital: any = {};
  icon: any;
  uid: any;
  segment: any = 'cloud';

  constructor(
    public helper: Helper,
    public hospitalService: HospitalService,
    public modalCtrl: ModalController,
    public nav: NavParams,
    public storage: Storage,
    public viewCtrl: ViewController,
  ) {
    this.init();
    }

  init(){
    this.helper.showLoading();
    let cloudhospitals = this.hospitalService.hospitals.getValue();
    this.hospitalService.getUserHospitals()
          .then((userHospitals: any) => {
            this.localHospitals = userHospitals;

            // mark custom user selected hospital
            _.each(this.localHospitals, function(local: any) {
              if (local.isRemoved != 'true' && local.approvalStatus == 'approved') {
                local.isSelected = true;
              }
            })
            return this.resetPreSelected(cloudhospitals)
          })
          .then((cloudhospitals) => {
            return this.checkSelected(cloudhospitals, this.localHospitals)
          })
          .then((cloudhospitals: any) => {
            this.cloudhospitals = cloudhospitals;
            this.updateHospitals();
            this.helper.hideLoading();
          })
          .catch((err: any) => {
            this.helper.hideLoading();
            console.log('err occured', err)
          })
  }
  // map selected hospital in local hospital list
  checkSelected(cloudHospital: any, localHospital: any) {
    return new Promise((resolve) => {
      _.each(localHospital, function(local: any) {
        _.each(cloudHospital, function(cloud: any) {
          if ((cloud.hospitalId == local.hospitalId)) {
            cloud.isSelected = true;
            cloud.icon = '';
          }
        })
      });
      resolve(cloudHospital);
    })
  }

  getApprovalStatusText(status){
    let text:any = '';
    switch(status){
      case "pending": text = 'waiting for review'
                      break;
      case "rejected": text = 'request rejected'
                      break;
      case "approved": text = 'request approved'
                      break;
      default: text = ''
               break;
    }
    return text;
  }

  resetPreSelected(hospitals: any) {
    return new Promise((resolve) => {
      _.each(hospitals, function(value: any) {
        value.icon = '';
        value.isSelected = false;
      });
      resolve(hospitals);
    })
  }

  updateHospitals() {
    this.selectedHospital = {};
    this.hospitals = this.segment == 'cloud' ? Object.assign([], this.cloudhospitals) : this.localHospitals;
  }

  async selectHospital() {
    let tempHospital:any;
    if (this.segment == 'local') {
      tempHospital = this.selectedHospital.hospitalDetail;
      tempHospital.hospitalId = this.selectedHospital.hospitalId;
      tempHospital.isCustom = true;
    } else {
      tempHospital = this.selectedHospital;
    }
    this.viewCtrl.dismiss();
    let aliasId:string = await this.storage.get('aliasId'),
    aliasName:string = await this.storage.get('aliasName'),
    role:any = await this.storage.get('role');
    tempHospital.userAliasId = aliasId;
    tempHospital.userAliasName = aliasName;
    tempHospital.userType = role;
    this.hospitalService.createUserHospital(tempHospital)
    .then(()=>{
      this.helper.showMessage('Hospital added successfully');
    })
    .catch((err)=>{
      console.log('err occured',err)
    })

    // let modal = this.modalCtrl.create(AddHospital, { hospitaldata: tempHospital });
    // modal.present();
    //
    // modal.onDidDismiss((data: any) => {
    //   if (data) {
    //     this.viewCtrl.dismiss();
    //   }
    // });
  }

  isValid() {
    return _.isEmpty(this.selectedHospital);
  }

  checkItem(hospital: any) {
    if(hospital.approvalStatus && hospital.approvalStatus != 'approved' && this.segment == 'local'){
      return false
    }
    let tempHospitals = this.segment == 'cloud' ? this.cloudhospitals : this.localHospitals;
    if (!hospital.isSelected) {
      _.each(tempHospitals, function(value: any) {
        value.icon = '';
      });
      this.selectedHospital = hospital;
      return hospital.icon = 'md-checkmark';
    }
  }

  getAddress(hospital: any) {
    return this.helper.getAddress(hospital);
  }

  isSelected(hospital: any) {
    if (hospital.isSelected || (hospital.isCustom && hospital.isRemoved != 'true' && hospital.approvalStatus == 'approved')) {
      return 'selected-hospital'
    }
    else {
      return ''
    }
  }

  createHospital() {
    let modal = this.modalCtrl.create(CreateHospital);
    modal.present();

    modal.onDidDismiss((data: any) => {
      this.init();
      if (data) {
        this.viewCtrl.dismiss(data);
      }
    })
  }

}
