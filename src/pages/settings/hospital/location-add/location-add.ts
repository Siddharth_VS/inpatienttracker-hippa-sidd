import { Component, NgZone } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';
import { StorageService } from '../../../../providers/storage-service';

@Component({
  selector: 'page-location-add',
  templateUrl: 'location-add.html',
})

export class AddLocationPage {

  lat: any;
  lng: any;
  location: any = {};
  autocompleteItems: any = [];
  autocomplete: any = { query: '' };
  service = new google.maps.places.AutocompleteService();

  constructor(
    public viewCtrl: ViewController,
    private zone: NgZone,
    public navParams: NavParams,
    private storageService: StorageService
  ) {}

  ionViewDidLoad() {
    setTimeout(async() => {
          this.location = {latitude: 28.951498, longitude: -82.625483};
          this.lat = this.navParams.get('lat') || await this.storageService.getSettings('CURRENT_LOCATION_LAT');
          this.lng = this.navParams.get('lng') || await this.storageService.getSettings('CURRENT_LOCATION_LNG');
          this.initMap();
    }, 500)
  }
  chooseItem(item: any) {
    this.autocomplete = {
      query: ''
    };
    this.autocompleteItems = [];

    let geo = new google.maps.Geocoder;
    let self = this;
    geo.geocode({ 'address': item }, function(result, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        self.lat = result[0].geometry.location.lat();
        self.lng = result[0].geometry.location.lng();
        self.initMap();
      }
    });
  }

  updateSearch() {
    if (this.autocomplete.query == '') {
      this.autocompleteItems = [];
      return;
    }
    let self = this;
    this.service.getPlacePredictions({ input: this.autocomplete.query }, function(predictions, status) {
      self.autocompleteItems = [];
      self.zone.run(function() {
        if (predictions) {
          predictions.forEach(function(prediction) {
            self.autocompleteItems.push(prediction.description);
          });
        }
      });
    });
  }


  initMap() {
    let self = this;

    let map = new google.maps.Map(document.getElementById('map'), {
      zoom: 17,
      center: { lat: parseFloat(self.lat), lng: parseFloat(self.lng) },
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    let marker = new google.maps.Marker({
      position: { lat: parseFloat(self.lat), lng: parseFloat(self.lng) },
      map: map,
      draggable: true,
      icon: 'assets/icon/icon-green.png'
    });

    marker.addListener('dragend', function() {
      let x = this.position;
      self.lat = x.lat();
      self.lng = x.lng();
      window.setTimeout(function() {
        map.panTo(marker.getPosition());
      }, 1000);
    });
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  saveLocation() {
    this.viewCtrl.dismiss(this.lat, this.lng);
  }

}
