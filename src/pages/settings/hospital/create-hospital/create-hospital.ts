import { Component } from '@angular/core';
import { ModalController, NavParams, ViewController } from 'ionic-angular';
import { NgForm } from '@angular/forms';
import { Helper } from '../../../../providers/helper';
import { AddHospital } from '../add-hospital/add-hospital';
import { AddLocationPage } from '../location-add/location-add';
import { HospitalService } from '../../../../providers/hospitalService';
import { StorageService } from '../../../../providers/storage-service';
import * as moment from 'moment';

@Component({
  selector: 'create-hospital',
  templateUrl: 'create-hospital.html'
})

export class CreateHospital {
  hospital: any = {};
  hospitalData:any = {};
  isEdit: boolean =  false;

  constructor(
    public helper: Helper,
    public hospitalService: HospitalService,
    public modalCtrl: ModalController,
    public navParams: NavParams,
    public storageService: StorageService,
    public viewCtrl: ViewController,
  ) { }

  ionViewDidLoad(){
    if(this.navParams.get('hospital')){
      this.hospitalData = Object.assign({},this.navParams.get('hospital').hospitalDetail, this.navParams.get('hospital'))
      delete this.hospitalData.hospitalDetail;
      this.hospital = this.hospitalData;
      this.isEdit = true;
    }
  }

  isFormValid(form:NgForm){
    return (form.valid && (this.hospital.lat || "").length && (this.hospital.long || "").length)
  }

  async saveHospital() {
    // this.hospital.createdBy = this.uid;
    if(!this.isEdit){
      this.helper.showLoading('Creating hospital, please wait...');
      this.hospital.code = 'CARD_NOT_SUPPORTED';
      this.hospital.isCustom = 'true';
      this.hospital.hospitalId = moment().format('x');
      this.hospital.createdByEmail = await this.storageService.getSettings('email');
      this.hospital.approvalStatus = 'pending'
      // this.hospital.createdDate = moment().format('');

      this.hospitalService.createUserHospital(this.hospital)
        .then(() => {
          return this.helper.sendEmail(this.hospital.createdByEmail,{operationType: "requestForApproval", hospital: this.hospital})
        })
        .then((response:any)=>{
          console.log('response',response)
          let res = typeof(response._body) == 'string' ? (JSON.parse(response._body)) : {};
          this.helper.hideLoading();
          if(res && res.status == 'Success'){
            this.viewCtrl.dismiss();
            this.helper.showMessage('Hospital created successfully, and added to Custom Hospitals', 3000)
          }else{
            this.viewCtrl.dismiss();
            this.helper.showMessage('Failed to create the custom hospital', 3000)
          }
        })
        .catch((err: any) => {
          console.log('err occured', err)
          this.helper.hideLoading();
          this.helper.showMessage('Failed to create the custom hospital', 3000)
        })
    }else{
      this.helper.showLoading();
      return this.hospitalService.updateUserHospitalDetail(this.hospital)
            .then(() => {
              this.helper.showMessage('Hospital created successfully, and added to Custom Hospitals', 3000)
              this.helper.hideLoading();
              this.viewCtrl.dismiss();
            })
            .catch((err: any) => {
              this.helper.hideLoading();
              this.helper.showMessage('Failed to update the custom hospital', 3000)
              console.log('err occured', err)
            })
    }

  }

  openLocationSelection(){
      let modal = this.modalCtrl.create(AddLocationPage, {lat: this.hospital.lat, lng: this.hospital.long});
      modal.present();
      modal.onDidDismiss((lat,lng) => {
        if(lat && lng){
          this.hospital.lat = "" + lat;
          this.hospital.long = "" + lng;
        }
      });
  }

}
