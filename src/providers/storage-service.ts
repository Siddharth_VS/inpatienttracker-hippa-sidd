import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Events, Platform } from 'ionic-angular';
// import { Badge } from '@ionic-native/badge';
import * as _ from 'underscore';
import { Apollo } from 'apollo-angular';
import { createSettings, getRolesData, getUserRoleSettings, getUserSettings, listPermissions, listRoles, updateSettings } from '../queries/settings';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';

declare const cordova: any;

@Injectable()
export class StorageService {
  constructor(
    public platform: Platform,
    public storage: Storage,
    // public badge: Badge,
    public events: Events,
    public apollo: Apollo
  ) { }

  getSettings(key: any) {
    return this.storage.get(key);
  }

  getUid(): Promise<string> {
    return this.storage.get("uid")
      .then(value => {
        return value;
      });
  }

  upsertApollo(key: any, value: any) {
    return new Promise((resolve, reject) => {
      const data: any = {};
      data[key] = value;
      this.apollo.mutate({ mutation: updateSettings, variables: data })
        .take(1)
        .subscribe((value: any) => {
          resolve(value);
        }, (err) => {
          console.log(err);
          reject(err);
        })
    })
  }

createUserSettings(key: any, value: any) {
  return new Promise((resolve, reject) => {
    const data: any = {};
    data[key] = value;
    this.apollo.mutate({ mutation: createSettings, variables: data })
      .take(1)
      .subscribe((value: any) => {
        resolve(value);
      }, ()=>{
        resolve(value);
      })
  })
}

getAllSettings() {
  return new Promise((resolve) => {
    this.apollo.watchQuery({ query: getUserSettings, fetchPolicy: 'network-only' }).valueChanges
      .pipe(map(result => result.data['getUserSettings'])).subscribe(data => {
        const copied = JSON.parse(JSON.stringify(data));
        resolve(copied);
      });
  })
}

// function to set settings locally
// passcode(unencrypted), specializationId, hospitalId
// Input shall be > PASSCODE, SPECIALIZATION, HOSPITAL_ID AND respective values'
setLocalSettings(key: any, value: any) {
  return this.storage.set(key, value);
}

  // function to set settings param in cloud
  // passcode(unencrypted), specializationId, hospitalId
  // Input shall be > PASSCODE, SPECIALIZATION, HOSPITAL_ID AND respective values'
  setSettings(key: any, value: any) {
    return new Promise((resolve, reject) => {
      this.getUid()
        .then(() => {
          return this.upsertApollo(key, value);
        })
        .then(() => {
          return this.setLocalSettings(key, value);
        })
        .then(() => {
          resolve();
        })
        .catch((err: any) => {
          reject();
        });
    });
  }

  //function to get the roles set for the specified user
  getUserRoleSettings(){
    return new Promise((resolve) => {
        this.apollo.watchQuery({ query: getUserRoleSettings, fetchPolicy: 'network-only' }).valueChanges
          .pipe(map(result => result.data['getUserRoleSettings'])).subscribe(data => {
            const copied = JSON.parse(JSON.stringify(data));
            resolve(copied);
          });
      })
    }

  //function to get the roles set for the highlevel role not for any specified user
  getRolesData(data:any){
    return new Promise((resolve) => {
        this.apollo.watchQuery({ query: getRolesData, variables:data, fetchPolicy: 'network-only' }).valueChanges
          .pipe(map(result => result.data['getRoles'])).subscribe(data => {
            const copied = JSON.parse(JSON.stringify(data));
            resolve(copied);
          });
      })
    }

  //function to list all the roles
  listRoles(){
    return new Promise((resolve) => {
        this.apollo.watchQuery({ query: listRoles, fetchPolicy: 'network-only' }).valueChanges
          .pipe(map(result => result.data['listRoles'])).subscribe(data => {
            const copied = JSON.parse(JSON.stringify(data));
            resolve(copied);
          });
      })
    }

  //function to list all the permissions higer level managed by Admin
  listPermissions(){
    return new Promise((resolve) => {
        this.apollo.watchQuery({ query: listPermissions, fetchPolicy: 'network-only' }).valueChanges
          .pipe(map(result => result.data['listPermissions'])).subscribe(data => {
            console.log('data',data)
            // const copied = JSON.parse(JSON.stringify(data));
            resolve(data);
          });
      })
    }
}
