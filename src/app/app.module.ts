import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { Auth } from 'aws-amplify';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { AccountPage } from '../pages/account/account';
import { WelcomePage } from '../pages/welcome/welcome';
import { SwitchHospital } from '../pages/switch-hospital/switch-hospital';
import { ArchivePage } from "../pages/archives/archives";
import { EditAccountPage } from "../pages/account/edit-account/edit-account";

// components
import { LockScreenComponent } from '../components/lock-screen/lock-screen';
import { HospitalCard } from '../components/hospital-card/hospital-card';
import { SecurityCard } from '../components/security-card/security-card';
import { SpecializationCard } from '../components/specialization-card/specialization-card';
import { NoResults } from '../components/no-results/no-results';
import { FooterMenu } from '../components/footer-menu/footer-menu';
import { TermsPage } from '../components/terms-policy/terms';
import { PolicyPage } from '../components/terms-policy/policy';
import { BoundingBoxLines } from '../components/bb-lines/bb-lines';
import { BoundingBoxWords } from '../components/bb-words/bb-words';
import { CardPreview } from '../components/card-preview/card-preview';

import { PatientDetailPage } from '../pages/patient-details/patient-details';

// pages
import { SettingsPage } from '../pages/settings/settings';
import { HospitalPage } from '../pages/settings/hospital/hospital';
import { HospitalList } from '../pages/settings/hospital/hospital-list/hospital-list';
import { AddHospital } from '../pages/settings/hospital/add-hospital/add-hospital';
import { CreateHospital } from '../pages/settings/hospital/create-hospital/create-hospital';
import { SecurityPage } from '../pages/settings/security/security';
import { AddEntitiyPage } from '../pages/add-entity/add-entity';
import { AddPatientPage } from '../pages/add-patient/add-patient';
import { AnalyticsPage } from "../pages/analytics/analytics";
import { SpecializationPage } from "../pages/settings/specialization/specialization";
import { BilledRecordPage } from '../pages/archives/billed-record/billed-record';
import { EntityLookupPage } from '../pages/entity-lookup/entity-lookup';
import { CartList } from '../pages/cart/cart-list/cart-list';
import { CartDetail } from '../pages/cart/cart-detail/cart-detail';
import { AddLocationPage } from '../pages/settings/hospital/location-add/location-add';
import { RecordSettingsPage } from '../pages/settings/record-settings/record-settings';
import { ManageHospitalsPage } from '../pages/manage-hospitals/manage-hospitals';
import { RolesManagementPage } from '../pages/roles-management/roles-management';
import { RelationsListPage } from '../pages/relations-list/relations-list';
import { SecretaryBilling } from '../pages/secretary-billing/secretary-billing';
import { CreateRelationsPage } from '../pages/relations-list/create-relations/create-relations';
import { HospitalCardsListing } from '../pages/settings/cards-management/cards-listing/cards-listing';
import { ManageCard } from '../pages/settings/cards-management/manage-card/manage-card';
import { BillDetails } from '../pages/bill-details/bill-details';

// Authentication
import { RegisterComponent } from "../pages/authentication/register/register";
import { RegistrationConfirmationComponent } from "../pages/authentication/confirm-registration/confirm-registration";
import { LoginComponent } from "../pages/authentication/login/login";
import { ForgotPasswordStepComponent } from "../pages/authentication/forgot-password/forgot-password";
import { VerifyPasswordComponent } from "../pages/authentication/forgot-password/verify-password/verify-password";

//plugins
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Keyboard } from '@ionic-native/keyboard';
import { Device } from '@ionic-native/device';
import { Network } from '@ionic-native/network';
import { IonicStorageModule } from '@ionic/storage';
import { TouchID } from '@ionic-native/touch-id';
import { EmailComposer } from '@ionic-native/email-composer';
import { File } from '@ionic-native/file';
import { Camera } from "@ionic-native/camera";
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Badge } from '@ionic-native/badge';
import { Geolocation } from '@ionic-native/geolocation';

// Providers
import { Helper } from '../providers/helper';
import { StorageService } from '../providers/storage-service';
import { PatientService } from '../providers/patient-service';
import { ApolloService } from '../providers/apollo-service';
import { SpecializationService } from '../providers/specialization-service';
import { PrinterService } from '../providers/printer-service';
import { AnalyticsService } from '../providers/analytics-service';

//Pipes
import { SearchPipe } from '../pipes/search-filter';

//directives
import { PermissionDirective } from '../directives/permission'

import appSyncConfig from './aws-export';
import AWSAppSyncClient from 'aws-appsync';
import Amplify from 'aws-amplify';

import { ApolloClient } from 'apollo-client';
import { ApolloModule, Apollo } from 'apollo-angular';
import { HttpLinkModule, HttpLink } from 'apollo-angular-link-http';
import { HttpClientModule, HttpHeaders } from "@angular/common/http";
import { HttpModule } from '@angular/http';
import { GraphqlService } from './graphql.service';
import { HospitalService } from '../providers/hospitalService';

//Subscription module
import { SubscriptionModule, ManageSubscriptions, SubscriptionHelperSerivce, SubscriptionListing } from '../subscriptions-module';
//amplify configuration
Amplify.configure(appSyncConfig);


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    WelcomePage,
    SettingsPage,
    HospitalPage,
    HospitalCard,
    HospitalList,
    AddHospital,
    CreateHospital,
    SecurityPage,
    AddEntitiyPage,
    AddPatientPage,
    SecurityCard,
    PatientDetailPage,
    AccountPage,
    AnalyticsPage,
    SpecializationPage,
    RegisterComponent,
    SearchPipe,
    LoginComponent,
    LockScreenComponent,
    RegistrationConfirmationComponent,
    VerifyPasswordComponent,
    ForgotPasswordStepComponent,
    SwitchHospital,
    SpecializationCard,
    CartList,
    CartDetail,
    ArchivePage,
    NoResults,
    BilledRecordPage,
    FooterMenu,
    EntityLookupPage,
    EditAccountPage,
    TermsPage,
    PolicyPage,
    AddLocationPage,
    RecordSettingsPage,
    ManageHospitalsPage,
    PermissionDirective,
    RolesManagementPage,
    RelationsListPage,
    CreateRelationsPage,
    HospitalCardsListing,
    ManageCard,
    BoundingBoxLines,
    BoundingBoxWords,
    CardPreview,
    SecretaryBilling,
    CreateRelationsPage,
    BillDetails
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    HttpLinkModule,
    IonicModule.forRoot(MyApp, {
      tabsHideOnSubPages: true,
      mode: 'ios',
      scrollAssist: true,
      autoFocusAssist: false,
      scrollPadding: false,
      actionSheetEnter: 'action-sheet-slide-in'
    }),
    IonicStorageModule.forRoot(),
    ApolloModule,
    SubscriptionModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    WelcomePage,
    AccountPage,
    SettingsPage,
    HospitalPage,
    HospitalCard,
    HospitalList,
    AddHospital,
    CreateHospital,
    SecurityPage,
    AddEntitiyPage,
    AddPatientPage,
    SpecializationPage,
    AnalyticsPage,
    SecurityCard,
    PatientDetailPage,
    RegisterComponent,
    LoginComponent,
    LockScreenComponent,
    RegistrationConfirmationComponent,
    VerifyPasswordComponent,
    ForgotPasswordStepComponent,
    SwitchHospital,
    SpecializationCard,
    CartList,
    CartDetail,
    ArchivePage,
    NoResults,
    BilledRecordPage,
    FooterMenu,
    EntityLookupPage,
    EditAccountPage,
    SubscriptionListing,
    ManageSubscriptions,
    TermsPage,
    PolicyPage,
    AddLocationPage,
    RecordSettingsPage,
    ManageHospitalsPage,
    RolesManagementPage,
    RelationsListPage,
    CreateRelationsPage,
    HospitalCardsListing,
    ManageCard,
    BoundingBoxLines,
    BoundingBoxWords,
    CardPreview,
    SecretaryBilling,
    CreateRelationsPage,
    BillDetails
  ],
  providers: [
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    StatusBar,
    SplashScreen,
    HospitalService,
    HospitalCard,
    Helper,
    PatientService,
    ApolloService,
    Keyboard,
    Device,
    Network,
    StorageService,
    SpecializationService,
    PrinterService,
    TouchID,
    File,
    GraphqlService,
    EmailComposer,
    AnalyticsService,
    Camera,
    BarcodeScanner,
    SubscriptionHelperSerivce,
    Badge,
    Apollo,
    Geolocation
  ]
})
export class AppModule {
  public hydratedClient: any;

  constructor() { }

}
