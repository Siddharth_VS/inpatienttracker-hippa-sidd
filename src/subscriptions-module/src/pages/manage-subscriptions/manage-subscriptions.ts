import { Component } from '@angular/core';
import { AlertController, NavController } from 'ionic-angular';
import { SubscriptionHelperSerivce } from '../../providers/helper-service';
import { PaymentAddCard } from '../payment-add-card/payment-add-card';
import { StorageService } from '../../../../providers/storage-service';
import * as _ from 'underscore';
import * as moment from 'moment';
import { ManageSources } from '../manage-sources/manage-sources';

@Component({
  selector: 'manage-subscriptions',
  templateUrl: 'manage-subscriptions.html'
})

export class ManageSubscriptions {
  subscriptions: any[] = [];
  isSubscribed: boolean = false;
  stripeCustId: any;
  isReady: boolean = false;
  currentSubscribedPlan: any = {};
  moment = moment;
  subscriptionId: any = '';
  subscriptionPlanId: any = '';

  constructor(
    public alertCtrl: AlertController,
    public helper: SubscriptionHelperSerivce,
    public navCtrl: NavController,
    public storageService: StorageService
  ) {
    this.init();
    this.helper.getsubscriptionPlans()
      .then((response: any) => {
        if (JSON.parse(response._body) && JSON.parse(response._body).data) {
          this.subscriptions = JSON.parse(response._body).data;
        }
      })
      .catch((err) => {
        console.log('eror occured', err);
      })
  }

  init() {
    let self: any = this;
    this.helper.hideLoading();
    this.helper.showLoading();
    this.storageService.getSettings('STRIPE_CUST_ID')
      .then((stripeCustId: any) => {
        if (stripeCustId) {
          self.helper.getSubscribedPlans(stripeCustId)
            .then((response: any) => {
              if (JSON.parse(response._body) && JSON.parse(response._body).data) {
                let customerSubscriptions = JSON.parse(response._body).data;
                this.currentSubscribedPlan = _.find(customerSubscriptions, (sub: any) => {
                  return sub.status == 'trialing' || sub.status == 'active';
                });
                if (this.currentSubscribedPlan) {
                  this.isReady = this.isSubscribed = true;
                  this.helper.hideLoading();
                }
                else {
                  this.isReady = true;
                  this.isSubscribed = false;
                  this.helper.hideLoading();
                }
              }
              else {
                this.helper.hideLoading();
              }
            })
            .catch((err) => {
              console.log('err occured', err);
              this.helper.hideLoading();
            })

        }
      })
      .catch((err) => {
        console.log('err occured', err)
      })
  }

  subscribe(subscriptionData: any) {
    this.navCtrl.push(PaymentAddCard);
  }

  updateSubscription(planId: any) {
    if (this.currentSubscribedPlan) {
      this.helper.showLoading();
      this.helper.updateSubscription(this.currentSubscribedPlan.id, planId)
        .then((response: any) => {
          if (response && JSON.parse(response._body) && JSON.parse(response._body).body.code && JSON.parse(response._body).body.code == 200) {
            this.init();
            this.helper.hideLoading();
            this.helper.showMessage(JSON.parse(response._body).body.msg, 3000);
          }
          else if (response.status != 200) {
            this.init();
            this.helper.hideLoading();
            this.helper.showMessage(JSON.parse(response._body).body, 3000);
          }
          else {
            this.init();
            this.helper.hideLoading();
            this.helper.showMessage(JSON.parse(response._body).body.msg, 3000);
          }
        })
        .catch((err) => {
          console.log('err occured', err);
          this.helper.hideLoading();
        })
    }
  }

  changeSubscription(planId: any) {
    let newSubscription = _.find(this.subscriptions, { id: planId });
    if (newSubscription) {
      let msg = `Are you sure want to change subscription plan from ${this.currentSubscribedPlan.plan.nickname} to ${newSubscription.nickname}`
      this.alertCtrl.create({
        title: 'Update Subscription',
        message: msg,
        buttons: [
          {
            text: 'Cancel'
          },
          {
            text: 'Proceed',
            handler: () => {
              this.updateSubscription(planId);
            }
          }
        ]
      }).present();
    }
  }

  cancelSubscription() {
    this.helper.showLoading();
    this.helper.cancelSubsription(this.currentSubscribedPlan.id)
      .then((response: any) => {
        this.init();
        this.helper.hideLoading();
      })
      .catch((err) => {
        console.log('err occured', err);
        this.helper.hideLoading();
        let msg: any;
        if (err && err.body && JSON.parse(err._body) && JSON.parse(err._body).error) {
          let errorObj = JSON.parse(err._body).error
          msg = errorObj.message;
        }
        else {
          msg = 'Failed to cacncel subscription, please try after some time';
        }
        this.helper.showMessage(msg);
      })
  }

  reactivateSubscription() {
    this.helper.showLoading();
    this.helper.updateSubscription(this.currentSubscribedPlan.id, this.currentSubscribedPlan.plan.id)
      .then((response: any) => {
        this.init();
        this.helper.hideLoading();
      })
      .catch((err) => {
        this.helper.hideLoading();
        console.log('err occured', err)
      })
  }

  viewManageSources() {
    this.navCtrl.push(ManageSources);
  }
}
