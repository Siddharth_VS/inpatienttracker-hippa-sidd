import { NgModule, ModuleWithProviders } from '@angular/core';
import { SubscriptionListing } from './src/pages/subscription-listing/subscription-listing';
import { ManageSubscriptions } from './src/pages/manage-subscriptions/manage-subscriptions';
import { ManageSources } from './src/pages/manage-sources/manage-sources';
import { CardDetail } from './src/pages/manage-sources/card-detail/card-detail';
import { PaymentAddCard } from './src/pages/payment-add-card/payment-add-card';
import { SubscriptionHelperSerivce } from './src/providers/helper-service';
import { IonicModule } from 'ionic-angular';
import { NoResults } from '../components/no-results/no-results';
import { SubscriptionTermsPage } from './src/pages/subscription-terms-policy/subscription-terms-policy';
import { EnvironmentsConfig } from './src/environments/environments';

@NgModule({
  imports: [
    // Only if you use elements like ion-content, ion-xyz...
    IonicModule
  ],
  declarations: [
    // declare all components/ page(s) that your module uses
    SubscriptionListing,
    PaymentAddCard,
    ManageSubscriptions,
    ManageSources,
    CardDetail,
    SubscriptionTermsPage
  ],
  exports: [
    // export the component(s)/ page(s) that you want others to be able to use
    SubscriptionListing,
    PaymentAddCard,
    CardDetail
  ],
  entryComponents: [
    PaymentAddCard,
    ManageSources,
    CardDetail,
    NoResults,
    SubscriptionTermsPage
  ],
  providers:[
    EnvironmentsConfig
  ]
})
export class SubscriptionModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SubscriptionModule,
      providers: [SubscriptionHelperSerivce, EnvironmentsConfig]
    };
  }
}
