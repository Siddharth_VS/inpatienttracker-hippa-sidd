import gql from 'graphql-tag';

export const subscriptionToHospitals = gql`
subscription {
  onCreateCloudHospitals {
       id
       name
       code
       shortname
       address1
       address2
       city
       state
       zipcode
  }
}
`;
