import gql from 'graphql-tag';

export const listHospitals = gql`
query ListHospitals {
    listAllHospitals {
      items {
        hospitalId
        name
        code
        shortname
        address1
        address2
        lat
        long
        city
        state
        zipcode
      }
    }
  }
`;
