import gql from 'graphql-tag';

export const listAllSpecializations = gql`
query listAllSpecializations{
  listAllSpecializations{
    items{
      name
      specializationId
    }
  }
}
`;

export const getSpecialization = gql`
query getSpecialization($specializationId: String!){
  getSpecialization(specializationId:$specializationId){
    items {
      name
      specializationId
    }
  }
}
`;
