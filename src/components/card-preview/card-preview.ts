import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Storage } from 'aws-amplify';
import { ActionSheetController, NavController } from 'ionic-angular';
import { Helper } from '../../providers/helper';
import { HospitalService } from '../../providers/hospitalService';
import { ManageCard } from '../../pages/settings/cards-management/manage-card/manage-card';

@Component({
  selector: 'card-preview',
  templateUrl: 'card-preview.html'
})

export class CardPreview{
  @Input('params') params: any;
  @Output('cardSelectionEmitter') cardSelectionEmitter: EventEmitter<any> = new EventEmitter();
  @Output('reloadListing') reloadListing: EventEmitter<any> = new EventEmitter();
  imageSrc= '';

  constructor(
    public actionSheetCtrl: ActionSheetController,
    public navCtrl: NavController,
    public helper: Helper,
    public hospitalService: HospitalService
  ){}

  ngOnChanges(){
    console.log('this.params', this.params)
    if((this.params.card || {}).s3ImageKey){
      Storage.get(this.params.card.s3ImageKey)
    .then((result:any) => {
    console.log('result s3', result)
    this.imageSrc = result;
    })
    .catch(err => console.log(err));
    }
  }

  onCardSelection(){
    if(this.params.isCardScanning == true){
      this.cardSelectionEmitter.emit({card: this.params.card})
    }else if(this.params.screenType == 'pendingApprovalList'){
      console.log('this.params', this.params)
      this.navCtrl.push(ManageCard, { rawImage: [], hospitalId: this.params.currentHospital.hospitalId, image: this.imageSrc, currentHospital: this.params.currentHospital, cardMappingType: 'cardApproval', card: this.params.card, showBackButton: true });
    }
  }

  more(){
    let self:any = this;
    let buttons: any = [{
        text: 'Submit for review',
        handler: () => {
          self.submitForReview()
        }
      },{
        text: 'Delete Card',
        role: 'destructive',
        handler: () => {
          self.deleteCard()
        }
      },{
        text: 'Cancel',
        role: 'cancel'
      }
    ];
    if(this.params.card.approvalStatus == 'pending' || this.params.card.approvalStatus == 'rejected' ){
      buttons.splice(0, 1)
    }
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Cad Options',
      buttons: buttons
    });
    actionSheet.present();
  }

  deleteCard(){
    return new Promise((resolve, reject)=>{
      this.helper.showLoading();
      if(this.params.card.s3ImageKey){
        Storage.remove(this.params.card.s3ImageKey)
        .then((result:any) => {
        console.log('result s3', result)
        resolve();
        })
    .catch((err) => {
      reject(err)
    });
      }else{
        resolve();
      }
    })
    .then(()=>{
      return this.hospitalService.deleteCard({hospitalId: this.params.card.hospitalId, cardId: this.params.card.cardId});
    })
    .then((response)=>{
      console.log('response after deletion', response)
      this.helper.hideLoading();
      this.reloadListing.emit({operationType: 'deleteCard'})
    })
    .catch((err)=>{
      this.helper.hideLoading();
      this.helper.showMessage("Failed to delete the card, please try again later");
      console.log(err)
    })
  }

  editCard(){
    console.log('edit card called')
  }

  submitForReview(){
    this.helper.showLoading();
    this.helper.manageCard({cardData: this.params.card, operationType: 'requestForApproval', hospital: this.params.currentHospital })
    .then((res:any)=>{
      this.helper.hideLoading();
      this.reloadListing.emit({operationType: 'requestForApproval'})
      console.log('in manageCard', res)
    })
    .catch((err)=>{
      this.helper.hideLoading();
      console.log('err occured',err)
    })
  }
}
