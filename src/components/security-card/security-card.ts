import { Component, Output, EventEmitter } from '@angular/core';
import { AlertController, Platform, ActionSheetController } from 'ionic-angular';
import { Helper } from '../../providers/helper';
import { StorageService } from '../../providers/storage-service';
import { TouchID } from '@ionic-native/touch-id';
declare const cordova: any;

@Component({
  selector: 'security-card',
  templateUrl: 'security-card.html'
})

export class SecurityCard {
  @Output() updateOptions = new EventEmitter();

  buttonText: string = 'Set';
  PDFPasscode: string;
  uid: any;
  PDFPasscodeToggle: boolean;
  disableTouchId: boolean = true;
  touchId: boolean;
  lockOptions: any = { 0: 'Never', 1: '1 Minute', 5: '5 minutes', 10: '10 Minutes' };
  selectedOption: any = 0;

  constructor(
    public alert: AlertController,
    public helper: Helper,
    public platform: Platform,
    public touchID: TouchID,
    public storageService: StorageService,
    public actionSheetCtrl: ActionSheetController,
  ) {
    this.helper.isFingerprintAvailable()
      .then((res) => {
        this.disableTouchId = res != 'AVAILABLE';
      })
      .catch((err) => {
        console.log(err);
      })

    this.storageService.getSettings('PASSCODE')
      .then((passcode: any) => {
        if (passcode) {
          this.buttonText = 'Change';
          this.sendOptions();
        }
        return this.helper.getUid();
      })
      .then((uid: any) => {
        this.uid = uid;
        return this.storageService.getSettings('TOUCH_ID');
      })
      .then((touchId) => {
        this.touchId = touchId;
        return this.helper.storageService.getSettings('PDF_PASSCODE');
      })
      .then((passcode: any) => {
        if (passcode) {
          this.PDFPasscode = passcode || null;
          this.PDFPasscodeToggle = passcode ? true : false;
        }
        return this.storageService.getSettings('AUTO_LOCK_IN_MINUTES');
      })
      .then((selectedLockInminutes: any) => {
        this.selectedOption = selectedLockInminutes || 0;
      })
      .catch((err: any) => {
        console.log('ERROR occured ', err);
      })
  }

  showOptions(myEvent: any) {
    let actionSheet = this.actionSheetCtrl.create({
      buttons: [
        {
          text: 'Never',
          cssClass: 'sort-buttton',
          handler: () => {
            this.selectAutoLockOptions(0);
          }
        },
        {
          text: '1 Minute',
          handler: () => {
            this.selectAutoLockOptions(1);
          }
        },
        {
          text: '5 Minutes',
          handler: () => {
            this.selectAutoLockOptions(5);
          }
        },
        {
          text: '10 Minutes',
          handler: () => {
            this.selectAutoLockOptions(10);

          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
        }
      ]
    });
    actionSheet.present();
  }

  selectAutoLockOptions(selectedOption: any) {
    this.selectedOption = selectedOption;
    this.storageService.setSettings('AUTO_LOCK_IN_MINUTES', selectedOption)
      .then(() => {
        this.helper.showMessage('Lock option set successfully', 2000, 'toast-success');
      })
      .catch((err: any) => {
        console.log('Err occured ', err);
      });
  }

  sendOptions() {
    this.updateOptions.emit({
      security: true
    });
  }

  setPdfPasscode() {
    if (this.PDFPasscodeToggle) {
      let self = this;
      let pin = '';
      this.helper.askPin('Passcode: minimum 4 digits ', 'Create new passcode', true)
        .then((newPin: any) => {
          pin = newPin;
          return self.helper.askPin('Passcode: minimum 4 digits', 'Confirm Passcode', true)
        })
      .then((confirmPin: any) => {
          if (pin !== confirmPin) {
            this.PDFPasscodeToggle = null;
            self.helper.showMessage("Passcode do not match, try again");
          } else {
            this.helper.showLoading("Saving PDF passcode");
            this.storageService.setSettings('PDF_PASSCODE', confirmPin)
              .then(() => {
                this.PDFPasscode = confirmPin;
                this.PDFPasscodeToggle = this.PDFPasscode ? true : false;
                this.helper.hideLoading();
                this.helper.showMessage('PDF Passcode added', 2000, 'toast-success');
              })
              .catch((err: any) => {
                this.helper.hideLoading();
                console.log(err);
              });
          }
        })
        .catch((err: any) => {
          this.PDFPasscodeToggle = false;
          console.log(err);
        });
    }
  }

  enablePdfToggle(event: any) {
    if (this.PDFPasscodeToggle && !this.PDFPasscode) {
      this.PDFPasscodeToggle = true;
      this.setPdfPasscode();
    }
    else if (!this.PDFPasscodeToggle && this.PDFPasscode) {
      this.helper.showLoading("Removing PDF passcode");
      this.storageService.setSettings('PDF_PASSCODE', null)
        .then(() => {
          this.PDFPasscode = null;
          this.helper.hideLoading();
          this.helper.showMessage('Passcode disabled successfully', 2000, 'toast-success');
        })
    }
  }

  updateTouchID(event: any) {
    this.touchId = event;
    this.storageService.setSettings('TOUCH_ID', this.touchId)
      .then(() => {
        let message = this.touchId ? 'Touch ID enabled successfully' : 'Touch ID disabled';
        this.helper.showMessage(message, 3000);
      });
  }

  updatePasscode() {
    let self = this;
    this.helper.askPin('Passcode: 4 digit', 'Create new passcode')
      .then((newPin: any) => {
        self.helper.askPin('Passcode: 4 digit', 'Confirm Passcode')
          .then((confirmPin: any) => {
            if (newPin !== confirmPin) {
              self.helper.showMessage("Passcode do not match, try again");
            } else {
              this.storageService.createUserSettings('APP_READY', "false")
                .then(() => {
                  self.storageService.setSettings('PASSCODE', confirmPin)
                })
                .then(() => {
                  this.buttonText = 'Change';
                  this.sendOptions();
                  this.helper.showMessage('Passcode updated successfully', 2000, 'toast-success');
                });
            }
          });
      });
  }

}
