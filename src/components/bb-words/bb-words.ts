import { Component, ElementRef, Input, Renderer2, ViewChild } from '@angular/core';
import * as _ from 'underscore';

@Component({
  selector: 'bb-word',
  template: `<span [ngClass]="getStyle()" #bbBorderContainer></span>`
})

export class BoundingBoxWords{

  @Input('params') params:any
  @ViewChild('bbBorderContainer') bbBorderContainer: ElementRef;

  constructor(
    public renderer: Renderer2
  ){}

  getStyle(){
    if(!(this.params || {}).word){
      return '';
    }
    this.renderer.setStyle(this.bbBorderContainer.nativeElement, "height", (this.params.word.Geometry.BoundingBox.Height * this.params.imageData.height) + 'px');
    this.renderer.setStyle(this.bbBorderContainer.nativeElement, "width", (this.params.word.Geometry.BoundingBox.Width * this.params.imageData.width) + 'px');
    this.renderer.setStyle(this.bbBorderContainer.nativeElement, "top", (this.params.word.Geometry.BoundingBox.Top * this.params.imageData.height) + 'px');
    this.renderer.setStyle(this.bbBorderContainer.nativeElement, "left", (this.params.word.Geometry.BoundingBox.Left * this.params.imageData.width) + 'px');
    if(this.params.containerType == 'boundingBox' || this.params.containerType == 'textExtraction'){
      this.bbBorderContainer.nativeElement.classList.remove('masked-ctr');
      this.bbBorderContainer.nativeElement.classList.add('bounding-box-ctr');
    }else if(this.params.containerType == 'masked'){
      let index = _.findIndex(this.params.attributes, (ele) => {
        return (ele.mappedData || {}).boundingArea && ele.mappedData.boundingArea.lineIndex == this.params.lineIndex && ele.mappedData.boundingArea.wordIndex == this.params.wordIndex;
      })
      if(index > -1 && (this.params.attributes[index] || {}).isMaskingRequired){
        this.bbBorderContainer.nativeElement.classList.remove('bounding-box-ctr');
        this.bbBorderContainer.nativeElement.classList.add('masked-ctr');
      }
      this.bbBorderContainer.nativeElement.classList.remove('bounding-box-ctr');
    }else{
      this.bbBorderContainer.nativeElement.classList.remove('masked-ctr');
      this.bbBorderContainer.nativeElement.classList.remove('bounding-box-ctr');
    }

    return ''
  }
}
