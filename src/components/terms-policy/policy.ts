import { Component } from '@angular/core';
import { ViewController } from 'ionic-angular';

@Component({
  selector: 'page-policy',
  templateUrl: 'policy.html'
})

export class PolicyPage {

  constructor(
    public viewCtrl: ViewController
  ) {

  }
}
