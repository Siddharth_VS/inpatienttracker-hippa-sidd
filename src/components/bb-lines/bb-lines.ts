import { Component, EventEmitter, Input, Output } from '@angular/core';
import * as _ from 'underscore';

@Component({
  selector: 'bb-line',
  template: `<bb-word *ngFor="let word of params.words;let i = index" [ngClass]="getClass(params.lineIndex, i)" [params]="{word: word, imageData: params.imageData, containerType: params.containerType, attributes: params.attributes, lineIndex: params.lineIndex, wordIndex: i}" (click)="select(word, params.lineIndex, i)"></bb-word>`
})

export class BoundingBoxLines{
  @Input('params') params:any
  @Output() selectionEmitter: EventEmitter<any> = new EventEmitter();

  constructor(){}

  select(word:any, lineIndex:number, wordIndex:number){
    if(this.params.containerType == 'textExtraction'){
      this.selectionEmitter.emit({ wordIndex, lineIndex, DetectedText: word.DetectedText, containerType: this.params.containerType, attributeIndex: this.params.currentSelectedAttribute.attributeIndex })
    }else{
      this.selectionEmitter.emit({ wordIndex, lineIndex, containerType: this.params.containerType })
    }
  }

  getClass(lineIndex:any, wordIndex:any){
    if(this.params.containerType == 'textExtraction'){
      if((this.params.currentSelectedBoxes || []).length){
        let index = _.findIndex(this.params.currentSelectedBoxes, (ele)=>{
          return ele.lineIndex == lineIndex && ele.wordIndex == wordIndex;
        })
        if((index > -1)){
          return 'chosen-word';
        }
      }
    }else{
      if(this.params.currentSelection && (lineIndex == this.params.currentSelection.lineIndex) && (wordIndex == this.params.currentSelection.wordIndex))  {
        return 'selected-word'
      }
    }
  return '';
  }
}
