import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search',
  pure: true
})

export class SearchPipe implements PipeTransform {
  objArray: any;
  transform(value: any, args: string, keys: any): any {
    if (!value || value && value.length < 1 || !args) {
      return value;
    }
    args = (args.trim()).replace(/ /g, '');
    this.objArray = value.filter(function(element: any) {
      let concatString: string = '';
      for (let i = 0; i < keys.length; i++) {
        if (element[keys[i]]) {
          concatString += element[keys[i]].toString().toLowerCase().replace(/ /g, '');
        }
      }
      if (concatString.indexOf(args.toLowerCase()) > -1) {
        return element;
      }
    });
    return this.objArray;
  }
}
