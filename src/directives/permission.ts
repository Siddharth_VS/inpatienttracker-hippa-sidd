import { Directive, Input, ElementRef, Renderer } from '@angular/core';
import { AngularFireOfflineDatabase } from 'angularfire2-offline/database';
import { Storage } from '@ionic/storage';
import { Helper } from '../providers/helper';
import _ from 'underscore';

@Directive({
  selector: '[permission]',
})
export class PermissionDirective {
  userRole: any;
  @Input('dataKeep')
  dataKeep: any;
  @Input('dataOmit')
  @Input('displayCss') displayCss:any
  dataOmit: any;
  keepPermissions: any;
  omitPermissions: any;
  observableInstance: any;
  constructor(
    public el: ElementRef,
    public renderer: Renderer,
    public storage: Storage,
    public helperService: Helper
  ) {}

   init() {
    try {
      let canShowElement: boolean = false;
      // Below Observable listens to subscription for updates in Roles.
      // Observable starts listening to roles in app.component and also when user is logged in
      this.observableInstance = this.helperService.permissionObservable.subscribe((allUserRoles: any) => {
        let userPermissions = allUserRoles;
        _.each(this.keepPermissions, (permission: string) => {
          if (permission && (userPermissions || []).includes(permission.trim())) {
            canShowElement = true;
          }
        });
        _.each(this.omitPermissions, (permission: string) => {
          if (permission && (userPermissions || []).includes(permission.trim())) {
            canShowElement = false;
          }
        });
        if (canShowElement) {
          this.renderer.setElementStyle(this.el.nativeElement, 'display', (this.displayCss || 'inherit'));
        } else {
          this.el.nativeElement.remove();
        }
      });
    } catch (err) {
      console.log(err);
    }
  }

  ngOnDestroy() {
    this.observableInstance && this.observableInstance.unsubscribe ? this.observableInstance.unsubscribe() : '';
    this.observableInstance = null;
  }

  ngOnChanges() {
    this.keepPermissions = _.compact((this.dataKeep || '').split(','));
    this.omitPermissions = _.compact((this.dataOmit || '').split(','));
    this.init();
  }
}
